<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['scani_description']	= 'Aplikasi anti plagiat <i>source code</i> menggunakan tools JPLAG yang mendukung bahasa pemograman <code>JAVA, C++/C#</code> dan <code>Python</code>';

/* End of file profiler_lang.php */
/* Location: ./system/language/english/profiler_lang.php */