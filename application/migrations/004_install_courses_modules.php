<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_courses_modules extends CI_Migration {
	
	

	public function __construct() {
		parent::__construct();
		
	}

	public function up() {
		
		$this->dbforge->drop_table('courses_modules', TRUE);

		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'course_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'title' => array(
				'type'       => 'VARCHAR',
				'constraint' => '225',
				'unique' => TRUE,
				'null'=>TRUE
			),
			'summary' => array(
				'type'       => 'text',
				'null'=>TRUE
			),
			'attachment' => array(
				'type' => 'text',
				'null'=>TRUE
			),
			'created_at' => array(
				'type'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'null'=>TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('courses_modules');		
	}

	public function down() {
		$this->dbforge->drop_table('courses_modules', TRUE);
		
	}
}
