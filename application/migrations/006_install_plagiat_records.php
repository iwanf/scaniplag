<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_plagiat_records extends CI_Migration {
	
	

	public function __construct() {
		parent::__construct();
		
	}

	public function up() {
		
		$this->dbforge->drop_table('plagiat_records', TRUE);

		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'BIGINT',
				'constraint'     => '20',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'plagiat_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'master' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE
			),
			'comparer' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE
			),
			'score' => array(
				'type' => 'DECIMAL',
				'constraint' => '5,3',
				'null' => TRUE
			),
			'created_at' => array(
				'type'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'null'=>TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('plagiat_records');		
	}

	public function down() {
		$this->dbforge->drop_table('plagiat_records', TRUE);
		
	}
}
