<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_courses extends CI_Migration {
	
	

	public function __construct() {
		parent::__construct();
		
	}

	public function up() {
		
		$this->dbforge->drop_table('courses', TRUE);

		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'lecturer_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'course_code' => array(
				'type'       => 'VARCHAR',
				'constraint' => '11',
				'unique' => TRUE,
				'null'=>TRUE
			),
			'course_name' => array(
				'type'       => 'VARCHAR',
				'constraint' => '225',
				'null'=>TRUE
			),
			'pretest_date' => array(
				'type'       => 'DATE',
				'null'=>TRUE
			),
			'postest_date' => array(
				'type'       => 'DATE',
				'null'=>TRUE
			),
			'amount_modules' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'last_meeting' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'amount_meetings' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'amount_students' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'created_at' => array(
				'type'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'null'=>TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('courses');		
	}

	public function down() {
		$this->dbforge->drop_table('courses', TRUE);
		
	}
}
