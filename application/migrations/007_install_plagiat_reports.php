<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_plagiat_reports extends CI_Migration {
	
	

	public function __construct() {
		parent::__construct();
		
	}

	public function up() {
		
		$this->dbforge->drop_table('plagiat_reports', TRUE);

		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'BIGINT',
				'constraint'     => '20',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'plagiat_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'file_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE
			),
			'similarity' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'indicator' => array(
				'type' => 'ENUM("red","yellow","green")',
				'null' => TRUE
			),
			'notes' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => TRUE
			),
			'system_score' => array(
				'type' => 'DECIMAL',
				'constraint' => '5,3',
				'null' => TRUE
			),
			'assistant_score' => array(
				'type' => 'DECIMAL',
				'constraint' => '5,3',
				'null' => TRUE
			),
			'lecturer_score' => array(
				'type' => 'DECIMAL',
				'constraint' => '5,3',
				'null' => TRUE
			),
			'output' => array(
				'type' => 'ENUM("true","false")',
				'null' => TRUE
			),
			'result' => array(
				'type' => 'ENUM("positif","negative")',
				'null' => TRUE
			),
			'plagiat' => array(
				'type' => 'ENUM("true","false")',
				'null' => TRUE
			),
			'result' => array(
				'type' => 'ENUM("positif","negative")',
				'null' => TRUE
			),
			'created_at' => array(
				'type'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'null'=>TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('plagiat_reports');		
	}

	public function down() {
		$this->dbforge->drop_table('plagiat_reports', TRUE);
		
	}
}
