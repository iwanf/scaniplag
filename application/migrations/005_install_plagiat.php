<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_plagiat extends CI_Migration {
	
	

	public function __construct() {
		parent::__construct();
		
	}

	public function up() {
		
		$this->dbforge->drop_table('plagiat', TRUE);

		$this->dbforge->add_field(array(
			'id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'course_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'module_id' => array(
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => TRUE,
				'null'       => TRUE
			),
			'mml' => array(
				'type' => 'INT',
				'constraint' => '3',
				'null' => TRUE
			),
			'language' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => FALSE
			),
			'sources' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => FALSE
			),
			'created_at' => array(
				'type'=>'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
				'null'=>TRUE
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('plagiat');		
	}

	public function down() {
		$this->dbforge->drop_table('plagiat', TRUE);
		
	}
}
