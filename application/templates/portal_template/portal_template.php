<!DOCTYPE html>
<html>
<head>
    <?php $CI->layout->trigger_title(); ?>
    <?php $CI->layout->trigger_charset(); ?>
    <?php $CI->layout->trigger_metadata(); ?>
    <?php $CI->layout->trigger_http_equiv(); ?>
    
    <?php $CI->layout->add_css_uri('css/uikit.min.css');?>
    <?php $CI->layout->add_css_uri('css/uikit-rtl.min.css');?>
    <?php $CI->layout->add_css_uri('font-awesome/css/font-awesome.min.css','local');?>
    <?php $CI->layout->trigger_css(); ?>
    
</head>
<body class="uk-height-1-1">
    <?= (isset($alert))? $alert : '' ?>
    <div class="uk-flex uk-flex-center uk-flex-middle uk-background-secondary uk-height-viewport uk-light">
        <div class="uk-position-bottom-center uk-position-small uk-visible@m">
            <span class="uk-text-small uk-text-muted">
                © 2018 Company Name - <a href="https://github.com/zzseba78/Kick-Off">Created by KickOff</a> | Built with <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a>
            </span>
        </div>
        <div class="uk-width-medium uk-padding-small">

         <?php $CI->layout->trigger_content_section('main'); ?>
     </div>
 </div>

 <?php $CI->layout->add_js_uri('js/jquery-1.12.4.js');?>
 <?php $CI->layout->add_js_uri('js/uikit.min.js');?>
 <?php $CI->layout->add_js_uri('js/uikit-icons.min.js');?>
 <?php $CI->layout->add_js_uri('js/scaniplag.js');?>
 <?php $CI->layout->trigger_js(); ?>

</body>
</html>