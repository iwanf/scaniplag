<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html> 
<head>
    <?php $CI->layout->trigger_title(); ?>
    <?php $CI->layout->trigger_charset(); ?>
    <?php $CI->layout->trigger_metadata(); ?>
    <?php $CI->layout->trigger_http_equiv(); ?>
    <?php $CI->layout->add_css_uri('css/uikit.min.css');?>
    <?php $CI->layout->add_css_uri('css/scaniplag.css');?>
    
    <?php $CI->layout->add_css_uri('font-awesome/css/font-awesome.min.css','local');?>
    
    <?php $CI->layout->trigger_css(); ?>
    <style>
    .uk-container-small {
        max-width: 1020px;
    }
</style>
</head>
<body onload="startTime()">

    <div class="uk-offcanvas-content">
        <!--HEADER-->
        <header>
            <div class="uk-container">
                <nav class="uk-navbar-transparent" data-uk-navbar>
                    <div class="uk-navbar-left">
                        <a class="uk-navbar-item uk-logo" href="#">
                            <img src="<?= base_url('public/images/logoittelkom.png') ?>" />
                        </a>
                    </div>
                    <div class="uk-navbar-center uk-visible@m">
                        <!--<ul class="uk-navbar-nav">
                            <li class="uk-active"><a href="#">Albums</a></li>
                            <li>
                                <a href="#">Collections <span data-uk-icon="icon: triangle-down"></span></a>
                                <div class="uk-navbar-dropdown">
                                    <ul class="uk-nav uk-navbar-dropdown-nav">
                                        <li><a href="#">Landscape</a></li>
                                        <li><a href="#">Nature</a></li>
                                        <li><a href="#">Social</a></li>
                                        <li><a href="#">Urban</a></li>
                                        <li><a href="#">Music</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="#">Most Viewed</a></li>
                        </ul>-->
                    </div>
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav">
                            <li>
                                <a href="<?= site_url('welcome/login') ?>" >
                                    <span data-uk-icon="icon:sign-in"></span>&nbsp;
                                    Masuk
                                </a>
                            </li>
                            <!--
                            <li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a></li>
                        -->
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!--/HEADER-->

    

    <!-- CONTENT -->

    <?= (isset($alert))? $alert : '' ?>
    <?php $CI->layout->trigger_content_section('main'); ?>
    
    <!-- /CONTENT -->
    <!--FOOTER-->
    <footer class="uk-section uk-section-default uk-section-small">
        <div class="uk-container">
            <p class="uk-text-small uk-text-center">Copyright <?= date('Y') ?> - ITTelkom Purwokerto. </p>
        </div>
    </footer>
    <!--/FOOTER-->
</div>
<div id="kb-snackbar"></div>
<!-- JS FILES -->
<?php $CI->layout->add_js_uri('js/jquery-1.12.4.js');?>
<?php $CI->layout->add_js_uri('js/uikit.min.js');?>
<?php $CI->layout->add_js_uri('js/uikit-icons.min.js');?>
<?php $CI->layout->trigger_js(); ?>
<script type="text/javascript">
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('scaniplag-clock').innerHTML =h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function copyQuote(id) {
    var target_copy = $("#q-"+id).text();
    setClipboard(target_copy);
    $('#kb-snackbar').text("Quotes berhasil tersalin :D");
    var x = document.getElementById("kb-snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}

$(document).ready(function() {
    const ui = UIkit,
    util = UIkit.util;
    var toShow = 1,
    switcher = ui.switcher(".uk-subnav", {
        animation: "uk-animation-slide-left-medium, uk-animation-slide-right-medium"
    });
    
    setInterval(() => {

        $("#btn-copy-quote").attr('onClick',"copyQuote("+toShow+")");
        switcher.show(toShow);
        toShow = (toShow + 1) % 3;

    }, 5000 );


});
</script>
</body>
</html>