<!DOCTYPE html>
<html>
<head>
    <?php $CI->layout->trigger_title(); ?>
    <?php $CI->layout->trigger_charset(); ?>
    <?php $CI->layout->trigger_metadata(); ?>
    <?php $CI->layout->trigger_http_equiv(); ?>
    
    <?php $CI->layout->add_css_uri('css/uikit.min.css');?>
    <?php $CI->layout->add_css_uri('css/dataTables.uikit.min.css');?>
    <?php $CI->layout->add_css_uri('summernote/summernote.css');?>
    <?php $CI->layout->add_css_uri('css/dashboard.css');?>
    <?php $CI->layout->add_css_uri('font-awesome/css/font-awesome.min.css','local');?>
    <?php $CI->layout->trigger_css(); ?>
</head>
<body>
    <div class="uk-offcanvas-content">
            <!-- MENU -->
            <?php $CI->layout->block('menu_block'); ?>
            <!-- /MENU -->
            <!-- CONTENT -->
            <div id="content" data-uk-height-viewport="expand: true">
                <div class="uk-container uk-container-expand">
                    <?= (isset($alert))? $alert : '' ?>
                    <?php $CI->layout->trigger_content_section('main'); ?>
                </div>
            </div>
            <!-- /CONTENT -->
            
        </div>
    <?php $CI->layout->include_template('hidden_partial'); ?>
    
    <!-- JS FILES -->
    <?php $CI->layout->add_js_uri('js/jquery-1.12.4.js');?>
    <?php $CI->layout->add_js_uri('summernote/summernote.js');?>
    <?php $CI->layout->add_js_uri('js/uikit.min.js');?>
    <?php $CI->layout->add_js_uri('js/uikit-icons.min.js');?>
    <?php $CI->layout->add_js_uri('js/jquery.dataTables.min.js');?>
    <?php $CI->layout->add_js_uri('js/dataTables.uikit.min.js');?>
    
    <?php $CI->layout->add_js_uri('js/scaniplag.js');?>
    <?php $CI->layout->trigger_js(); ?>
</body>
</html>