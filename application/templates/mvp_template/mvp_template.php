<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$CI =&get_instance();

if ($CI->ion_auth->is_admin()) {
	$menu = mvp_menu_admin();
}elseif ($CI->ion_auth->in_group('asisten')) {
	$menu = mvp_menu_asisten();
}else{
	$menu = array();
}

?>

<!DOCTYPE html>
<html>
<head>
	<?php $CI->layout->trigger_title(); ?>
	<?php $CI->layout->trigger_charset(); ?>
	<?php $CI->layout->trigger_metadata(); ?>
	<?php $CI->layout->trigger_http_equiv(); ?>
	
	<?php $CI->layout->add_css_uri('css/uikit.min.css');?>
	<?php $CI->layout->add_css_uri('css/dataTables.uikit.min.css');?>
	<?php $CI->layout->add_css_uri('summernote/summernote.css');?>
	<?php $CI->layout->add_css_uri('css/dashboard.css');?>
	<?php $CI->layout->add_css_uri('css/uikit.scaniplag.css');?>

	<?php $CI->layout->add_css_uri('font-awesome/css/font-awesome.min.css','local');?>
	<?php $CI->layout->trigger_css(); ?>
	<title>Scaniplag</title>
	<link rel="stylesheet" type="text/css" href="">
	<style type="text/css">
	#nav.uk-navbar-container {
		background: #3f51b5;
	}

	.uk-border-rounded {
		border-radius: 10px;
	}

	.uk-section-muted {
		background: #f0f8ff;
	}
	.uk-section{
		padding-top:20px;
	}
	h1 > small{
		font-size: 14px;
	}
</style>
</head>
<body>
	<div id="nav" class="uk-navbar-container">
		<div class="uk-container">
			<nav class="uk-margin uk-navbar uk-light" uk-navbar>
				<div class="uk-navbar-left">

					<ul class="uk-navbar-nav">
						<li>
							<?= anchor('dashboard/index',$CI->config->item('app_name')) ?>
						</li>
					</ul>

				</div>
				<div class="uk-navbar-center">

					<ul class="uk-navbar-nav">
						<?php if ($menu): ?>
							<?php foreach ($menu as $key => $value) { ?>
								<li>
									<a href="<?= site_url($value['url']) ?>">
										<span class="uk-margin-small-right" data-uk-icon="icon:<?= $value['icon'] ?>"></span>
										<?= ucwords($value['label']) ?>
									</a>
								</li>
							<?php } ?>
						<?php endif ?>
					</ul>

				</div>
				<div class="uk-navbar-right">
					<div class="uk-navbar-item">
						<div class="uk-inline">
							<button class="uk-button uk-button-default" type="button">
								<?php
								if (isset($CI->current_user['username'])) {
									echo ucwords($CI->current_user['username']);
								}
								?>
							</button>
							<div uk-dropdown>
								<ul class="uk-nav uk-dropdown-nav">
									<li class="uk-active">
										<a href="<?= site_url('dashboard/profile') ?>">
											<span class="uk-margin-small-right" data-uk-icon="icon:user"></span>&nbsp;
											Profile
										</a>
									</li>
									<!--<li>
										<a href="<?php //site_url('dashboard/pengaturan') ?>">
											<span class="uk-margin-small-right" data-uk-icon="icon:cog"></span>&nbsp;
											Pengaturan
										</a>
									</li>-->
									<li class="uk-nav-divider"></li>
									<li>
										<a href="<?= site_url('dashboard/logout') ?>" class="btn-confirm">
											<span data-uk-icon="icon: sign-out"></span> Keluar
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>

	<div class="uk-section uk-section-muted">
		<div class="uk-container">
			<?= (isset($alert))? $alert : '' ?>
			<?php $CI->layout->trigger_content_section('main'); ?>
		</div>
	</div>
	<?php $CI->layout->include_template('hidden_partial'); ?>
	
	<!-- JS FILES -->
    <?php $CI->layout->add_js_uri('js/jquery-1.12.4.js');?>
    <?php $CI->layout->add_js_uri('summernote/summernote.js');?>
    <?php $CI->layout->add_js_uri('js/uikit.min.js');?>
    <?php $CI->layout->add_js_uri('js/uikit-icons.min.js');?>
    <?php $CI->layout->add_js_uri('js/jquery.dataTables.min.js');?>
    <?php $CI->layout->add_js_uri('js/dataTables.uikit.min.js');?>
    
    <?php $CI->layout->add_js_uri('js/scaniplag.js');?>
    <?php $CI->layout->trigger_js(); ?>

</body>
</html>