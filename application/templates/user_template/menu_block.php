<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$CI 	=&get_instance(); 
	if ($CI->ion_auth->is_admin()) {
		$menu 	= get_admin_menu();
	}elseif ($CI->ion_auth->in_group('dosen')) {
		$menu 	= get_dosen_menu();
	}else{
		$menu 	= get_mahasiswa_menu();
	}
?>
<!--HEADER-->
<header id="top-head" class="uk-position-fixed">
	<div class="uk-container uk-container-expand uk-background-primary">
		<nav class="uk-navbar uk-light" data-uk-navbar="mode:click">
			<div class="uk-navbar-left">
				<?= anchor('dashboard/index',$CI->config->item('app_name'),array('class'=>'uk-navbar-item uk-logo')) ?>
				
				<!--<div class="uk-navbar-item uk-visible@s">
					<form action="dashboard.html" class="uk-search uk-search-default">
						<span data-uk-search-icon></span>
						<input class="uk-search-input search-field" type="search" placeholder="Search">
					</form>
				</div>-->
			</div>
			<div class="uk-navbar-right">
				<ul class="uk-navbar-nav">
					<li>
						<a href="#" data-uk-icon="icon:user"></a>
						<div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-left">
							<ul class="uk-nav uk-navbar-dropdown-nav">
								<!--<li class="uk-nav-header">
									Akunku
								</li>-->
								<li><a href=""><span data-uk-icon="icon: info"></span> Profile</a></li>
								<!--<li><a href="#"><span data-uk-icon="icon: album"></span> Berkas</a></li>-->
								<li class="uk-nav-divider"></li>
								<li>
									<a href="<?= site_url('dashboard/logout') ?>" class="btn-confirm">
										<span data-uk-icon="icon: sign-out"></span> Logout
									</a>
								</li>
								
							</ul>
						</div>
					</li>
					<!--<li><a href="<?php //site_url('dashboard/konfigurasi') ?>" data-uk-icon="icon: cog"></a></li>-->
				</ul>
			</div>
		</nav>
	</div>
</header>
<!--/HEADER-->
<!-- LEFT BAR -->
<aside id="left-col" class="uk-light uk-visible@m">
	<div class="profile-bar">
		<div class="uk-grid uk-grid-small uk-flex uk-flex-middle" data-uk-grid>
			<div class="uk-width-auto">
				<img src="<?=base_url('public/images/placeholder-user.png') ?>" class="uk-border-circle profile-img">
			</div>
			<div class="uk-width-expand">
				<span class="uk-text-small uk-text-muted">Hello</span>
				<p class="uk-margin-remove-vertical text-light">
					<?php
					if (isset($CI->current_user['username'])) {
						echo ucwords($CI->current_user['username']);
					}
					?>
				</p>
			</div>
		</div>
	</div>
	
	<div class="bar-content uk-position-relative">
		<ul class="uk-nav-default uk-nav-parent-icon" data-uk-nav>
			<?php
			if ($menu) {
				foreach ($menu as $key => $value) {
					if (isset($value['sub'])) {
						echo '<li class="uk-nav-header">'.ucwords($value['label']).'</li>';
						foreach ($value['sub'] as $index => $sub_menu) {
							echo '<li><a href="'.site_url($sub_menu['url']).'">';
							echo '<span class="uk-margin-small-right" data-uk-icon="icon: '.$sub_menu['icon'].'"></span>';
							echo ucwords($sub_menu['label']).'</a></li>';
						}
					}else{
						echo '<li><a href="'.site_url($value['url']).'">';
						echo '<span class="uk-margin-small-right" data-uk-icon="icon: '.$value['icon'].'"></span>';
						echo ucwords($value['label']).'</a></li>';
					}
				}
			}
			?>
		</ul>
	</div>
	<div class="uk-position-bottom bar-bottom">
		<ul class="uk-subnav uk-flex uk-flex-center uk-child-width-1-6" data-uk-grid>
			<li>
				<a href="#" class="uk-icon-link" data-uk-icon="icon: home"></a>
			</li>
			<li>
				<a href="#" class="uk-icon-link" data-uk-icon="icon: settings"></a>
			</li>
			<li>
				<a href="#" class="uk-icon-link" data-uk-icon="icon: social"></a>
			</li>
			<li>
				<a href="#" class="uk-icon-link" data-uk-icon="icon: comment"></a>
			</li>
			<li>
				<a href="#" class="uk-icon-link" data-uk-tooltip="Sign out" data-uk-icon="icon: sign-out"></a>
			</li>
		</ul>
	</div>
</aside>
			<!-- /LEFT BAR -->