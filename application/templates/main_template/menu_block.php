<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$CI =&get_instance(); 
?>
<header class="uk-box-shadow-small" style="background-color: white">
	<div class="uk-container uk-container-expand">
		<nav class="uk-navbar" id="navbar" data-uk-navbar>
			<div class="uk-navbar-left">
				<?= anchor('welcome/index',$CI->config->item('app_name'),array('class'=>'uk-navbar-item uk-logo')) ?>
			</div>

			<div class="uk-navbar-right">
				<ul class="uk-navbar-nav uk-visible@m">
					<li class="uk-active"><a href="#">Beranda</a></li>
					<li><a href="#more" title="More about Plans" data-uk-scroll="duration: 400">Fitur</a></li>
					<li><a href="#">Panduan</a></li>
				</ul>
				<div class="uk-navbar-item">
					<a class="uk-navbar-toggle uk-hidden@s" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
					<?= anchor('welcome/login','<i data-uk-icon="sign-in"></i> LOGIN',array('class'=>'uk-button uk-button-secondary uk-visible@m')) ?>
				</div>
			</div>
		</nav>
	</div>
</header>

