<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Dashboard extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();		
		if (!$this->ion_auth->get_user_id()) {
			redirect('welcome/index','refresh');
		}

	}

	public function index(){
		$user_id = $this->ion_auth->get_user_id();

		$this->load->model('users_groups');
		$this->load->model('courses');
		$this->load->model('courses_assistants');
		$this->load->model('plagiat');
		
		if ($this->ion_auth->is_admin()) {
			$jumlah_asisten 	= $this->users_groups->count_by('group_id',2);
			$jumlah_praktikum	= $this->courses->count_all();

			$data['pintas'] 	= array(
				array(
					'qty'=> $jumlah_asisten ,
					'icon'=>'happy',
					'unit'=>'Asisten',
					'url'=> site_url('admin/tambah_asisten'),
					'label'=>'Tambah Asisten',
				),
				array(
					'qty'=> $jumlah_praktikum,
					'icon'=>'laptop',
					'unit'=>'Praktikum',
					'url'=> site_url('admin/tambah_praktikum'),
					'label'=>'Tambah Praktikum'
				),
			);
		}else{
			$jumlah_praktikum	= $this->courses_assistants->count_by('user_id',$user_id);
			$jumlah_laporan		= $this->plagiat->count_un_checked_by($user_id);
			$data['pintas'] 	= array(
				array(
					'qty'=> $jumlah_praktikum,
					'icon'=>'laptop',
					'unit'=>'Praktikum',
					'url'=> site_url('asisten/submit_tugas'),
					'label'=>'Submit Tugas'
				),
				array(
					'qty'=> $jumlah_laporan,
					'icon'=>'mail',
					'unit'=>'Tugas tersubmit',
					'url'=> site_url('asisten/cek_plagiat'),
					'label'=>'Cek Plagiat'
				),
			);
		}
		$this->layout
		->set_template('mvp_template')
		->set_title('Admin - Beranda')
		->render_action_view($data);
	}


	public function logout(){
		if ($this->ion_auth->logout()) {
			$this->session->unset_userdata('user_id');
				//$this->session->sess_destroy();
			redirect('welcome/index','refresh');
		}
	}

	public function update_profile(){
		$this->load->model('ion_auth_model');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		if ($this->form_validation->run()) {
			$data = $this->input->post(NULL,true);
			if (isset($data['password'])) {
				$data['password'] = trim($data['password']);
				if(!empty($data['password']) && strlen($data['password']) > 4) {
					$this->form_validation->set_rules('password','Password','trim|required');
					$this->form_validation->set_rules('c_password','Konfirmasi password baru','trim|required|matches[password]');
					if ($this->form_validation->run()) {
						unset($data['c_password']);
					}else{
						unset($data['password']);
						unset($data['c_password']);
					}
				}
			}
			
			if (isset($this->current_user['username']) && isset($data['username'])) {
				if ($this->current_user['username'] !== $data['username']) {
					$this->form_validation->set_rules('username','Username','required|is_unique[users.username]');
					if ($this->form_validation->run()) {
						$this->users->update($user_id,array(
							'username'=>$data['username']
						));
					}
				}
			}
			
			if ($this->ion_auth_model->update($user_id,$data)) {
				$this->layout->set_alert('success','Profile berhasil diperbaharui');
			}else{
				$this->layout->set_alert('warning','Profile gagal diperbaharui :(');
			}

		}else{
			$this->layout->set_alert('warning',validation_errors());
		}
		go_back('profile');
	}

	public function profile()
	{
		$this->load->model('groups');
		$user_id = $this->current_user['id'];
		$data['profile'] 	= $this->current_user;
		$data['groups'] 	= $this->groups->get_id_groups_by_user_id($user_id);		
		$this->layout->set_template('mvp_template')
		->set_title('Profile')
		->render_action_view($data);	
	}
}