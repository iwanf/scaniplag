<?php
/**
 * 
 */
require APPPATH.'libraries/REST_Controller.php';

class Api extends REST_Controller
{
	protected $user_id;
	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->lang->load('auth');
		$this->user_id = $this->ion_auth->get_user_id();
		if (!$this->user_id) {
			//redirect('welcome/index','refresh');
		}
	}

	public function get_users_post()
	{
		$this->load->model('users');
		$this->users->fields = array(
			'username',
			'email',
			'company'
		);
		$options = array(
			'<span uk-icon="trash"></span>' => array('url'=>'admin/hapus_user','type'=>'danger'),
			'<span uk-icon="refresh"></span>' => array('url'=>'admin/reset_password','type'=>'default'),
			
		);
		$data = $this->users->get_datatables_format($options);
		$this->response($data);
	}

	public function get_files_get()
	{
		$this->load->helper('tools');
		$dirs 	= implode('\\', func_get_args());
		$data 	= array();
		if (!empty($dirs)) {
			$path   = APPPATH.$dirs;
			$data 	= get_files_in($path);
		}
		
		$this->response($data);
	}

	public function get_students_post($class_id)
	{
		$this->load->model('users');
		$data = $this->users->get_students($class_id);
		$this->response($data);
	}

	public function get_lecturers_post()
	{
		$this->load->model('users');
		$data = $this->users->get_lecturers();
		$this->response($data);
	}

	public function get_students_unregistered_post()
	{
		$this->load->model('students');
		$data = $this->students->get_unregistered();
		$this->response($data);
	}

	public function get_lecturers_unregistered_post()
	{
		$this->load->model('lecturers');
		$data = $this->lecturers->get_unregistered();
		$this->response($data);
	}

	public function get_courses_for_lecturer_post()
	{
		$this->load->model(array('courses','users'));
		$options = array(
			'<span uk-icon="more"></span> Details' => array('url'=>'dosen/details_matakuliah','type'=>'primary'),
		);
		$data['data'] 	= array();
		$user 			= $this->users->get($this->user_id);
		if (isset($user['lecturer_id'])) {
			$this->courses->fields = array(
				'course_code',
				'course_name',
				'pretest_date',
				'postest_date'
			);
			$data 		= $this->courses->get_by_lecturer($user['lecturer_id'],$options);
		}
		$this->response($data);	
	}

	public function get_modules_by_course_post($id)
	{
		$this->load->model('courses_modules');
		$data 	= $this->courses_modules->get_by_course($id);
		$items 	= array();
		if ($data) {
			$number = 1;
			foreach ($data as $key => $value) {
				if ($this->ion_auth->is_admin()) {
					$action  = anchor('admin/edit_modul/'.$value->id,'Edit',array('class'=>'uk-button uk-button-small uk-button-default'));
					$action .= anchor('admin/details_modul/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));
				}else{
					$action ='<a href="#modal-ringkasan-'.$value->id.'" class="uk-button uk-button-small uk-button-primary btn-ringkasan" uk-toggle>Ringkasan</a>';
				}
				$attachment = '<a href="'.base_url('modules/'.$value->attachment).'" target="_blank">'.$value->attachment.'</a>';
				$items[] = array(
					$number,
					$value->title,
					$attachment,
					$action
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);	
	}

	public function get_asisten_post()
	{
		$this->load->model('ion_auth_model');
		$data = $this->ion_auth_model->users(array(2))->result();
		$items = array();
		$number = 1;

		foreach ($data as $key => $value) {
			$action 	= anchor('admin/details_asisten/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));
			$items[] 	= array(
				$number,
				$value->email,
				$value->phone,
				$value->first_name.' '.$value->last_name,
				$action
			);
			$number++;
		}
		$json['data'] = $items;
		$this->response($json);	
	}

	public function get_praktikum_by_post($user_id='')
	{
		$this->load->model('courses_assistants');
		$this->load->helper('scaniplag');

		$courses 	= $this->courses_assistants->get_by_asisten($user_id);
		$items 		= array();
		if ($courses) {
			$number = 1;

			foreach ($courses as $key => $value) {
				
				if ($this->ion_auth->is_admin()) {
					$action  = anchor('admin/edit_praktikum/'.$value->id,'Edit',array('class'=>'uk-button uk-button-small uk-button-default'));
					$action .= anchor('admin/details_praktikum/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));
				}else{
					$action = anchor('asisten/details_praktikum/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));
				}

				
				$lecturer = '';
				if (!is_null($value->lecturer_id)) {
					$first_name = get_info_user($value->lecturer_id,'first_name');
					$last_name 	= get_info_user($value->lecturer_id,'last_name');
					$lecturer 	= ucfirst($first_name).' '.ucfirst($last_name);
				}
				$items[] = array(
					$number,
					$value->course_name,
					$lecturer,
					$value->amount_modules,
					$action
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);	
	}

	public function get_praktikum_post(){
		$this->load->helper('scaniplag');
		$this->load->model('courses');
		$courses 	= $this->courses->as_object()->get_all();
		$items		= array();
		if ($courses) {
			$number = 1;

			foreach ($courses as $key => $value) {
				$action  = anchor('admin/edit_praktikum/'.$value->id,'Edit',array('class'=>'uk-button uk-button-small uk-button-default'));
				$action .= anchor('admin/details_praktikum/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));

				$lecturer = '';
				if (!is_null($value->lecturer_id)) {
					$first_name = get_info_user($value->lecturer_id,'first_name');
					$last_name 	= get_info_user($value->lecturer_id,'last_name');
					$lecturer 	= ucfirst($first_name).' '.ucfirst($last_name);
				}
				$items[] = array(
					$number,
					$value->course_name,
					$lecturer,
					$value->amount_modules,
					$action
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);	
	}

	public function modules_praktikum_get($id='')
	{
		$this->load->model('courses_modules');
		$data 	= $this->courses_modules->get_by_course($id);
		$this->response($data);	
	}

	public function get_plagiat_by_asisten_post()
	{
		$this->load->model('plagiat');
		$data 	= $this->plagiat->get_join_by_user_id($this->user_id);
		
		$items 	= array();	
		if ($data) {
			$number = 1;
			foreach ($data as $key => $value) {
				if (is_null($value->mml) || $value->mml < 0) {
					$action = anchor('#modal-cek-plagiat','Cek plagiat',array('data-id'=>$value->id));
					$sources ='';
				}else{
					$action = anchor('asisten/details_submit/'.$value->id,'Details',array('class'=>'uk-button uk-button-small uk-button-primary'));
					$sources = anchor(base_url('results/'.$value->sources),$value->sources,array('target'=>'_blank'));
				}
				$items[] = array(
					$number,
					$value->course_name,
					$value->title,
					$value->language,
					$value->mml,
					$sources,
					$action
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);
	}

	public function get_report_by_plagiat_id_post($plagiat_id)
	{
		$this->load->model('plagiat_reports');
		$data 	= $this->plagiat_reports->get_many_by('plagiat_id',$plagiat_id);
		$items 	= array();	
		if ($data) {
			$number=1;
			
			foreach ($data as $key => $value) {
				if (is_null($value->assistant_score) || $value->assistant_score < 0) {
					$action = anchor('asisten/input_nilai/'.$value->id,'Input',array('class'=>'uk-button uk-button-small uk-button-primary'));
				}else{
					$action = anchor('asisten/input_nilai/'.$value->id,'Edit',array('class'=>'uk-button uk-button-small uk-button-default'));
				}
				
				$items[] = array(
					$number,
					$value->file_name,
					$value->system_score,
					$value->assistant_score,
					$value->lecturer_score,
					$action
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);
	}

	public function get_records_by_plagiat_id_post($plagiat_id)
	{
		$this->load->helper('tools');
		$this->load->model('plagiat_records');
		$data = $this->plagiat_records->get_by_plagiat_id($plagiat_id);
		$items 	= array();	
		if ($data) {
			$number=1;
			foreach ($data as $key => $value) {
				$items[] = array(
					$number,
					$value->master,
					$value->comparer,
					$value->score,
					show_date_human_format($value->created_at,true),
				);
				$number++;
			}
		}
		$json['data'] = $items;
		$this->response($json);
	}

	public function get_dosen_post(){
		$this->load->model('ion_auth_model');
		$data = $this->ion_auth_model->users(array(3))->result();
		$items = array();
		$number = 1;
		foreach ($data as $key => $value) {
			
			$items[] 	= array(
				$number,
				$value->email,
				$value->phone,
				$value->first_name.' '.$value->last_name,
			);
			$number++;
		}
		$json['data'] = $items;
		$this->response($json);	
	}
	
}