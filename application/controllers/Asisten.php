<?php
/**
 * 
 */
class Asisten extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->get_user_id()) {
			redirect('welcome/index','refresh');
		}
		$this->load->helper('scaniplag');
		$this->load->helper('tools');
	}

	public function index(){
		redirect('dashboard/index','refresh');
	}

	public function submit_tugas()
	{
		$this->load->model('courses_assistants');
		$data['languages'] = array(
			'c/c++'=>'C / C++',
			'java17'=>'JAVA',
			'python3'=> 'Python 3.*'
		);
		$user_id = $this->ion_auth->get_user_id();
		$data['courses'] = $this->courses_assistants->get_by_asisten($user_id);
		$this->layout
		->set_template('mvp_template')
		->set_title('Submit Tugas - Scaniplag')
		->render_action_view($data);
	}

	public function cek_plagiat()
	{
		$this->load->model('plagiat');
		$user_id = $this->ion_auth->get_user_id();
		$data['un_checked'] = $this->plagiat->get_un_checked_by($user_id);
		$data['languages'] = array(
			'c/c++'=>'C / C++',
			'java17'=>'JAVA',
			'python3'=> 'Python 3.*'
		);
		$this->layout
		->set_template('mvp_template')
		->set_title('Beranda')
		->render_action_view($data);
	}

	public function submit_cek_plagiat($plagiat_id='')
	{
		$this->load->model('plagiat');
		$this->load->model('plagiat_records');
		$plagiat = $this->plagiat->get($plagiat_id);
		if ($plagiat) {
			$data 		= $this->input->post(NULL,true);
			$mml 		= intval($data['mml']);

			$this->plagiat->update($plagiat_id,array(
				'mml' => $mml
			));

			if ($this->form_validation->run('submit_cek_plagiat')) {
				
				$sources_path 	= APPPATH.'extracts/'.$data['sources'];
				$result_path 	= 'results/'.$data['sources'].'/';
				$language 	 	= $data['language'];
				
				$command 	= "java -jar jplag.jar -l $language -t $mml -s $sources_path -r $result_path";
				$output 	= array(); 
				exec($command,$output);
				$records 	= get_output_jplag_cli($output);
				if ($records) {

					foreach ($records as $key => $value) {
						$series = array(
							'plagiat_id' => $plagiat_id,
							'master' => $value['master'],
							'comparer' => $value['comparer'],
							'score' => $value['score'],
						);
						$this->plagiat_records->insert($series);
					}
				}
				$this->layout->set_alert('success','Data berhasil tersimpan');
				redirect('asisten/details_plagiat/'.$plagiat_id,'refresh');	
			}else{
				$this->layout->set_alert('warning',validation_errors());
			}
		}else{
			$this->layout->set_alert('danger','Submit tugas tidak ditemukan');
		}
		go_back('cek_plagiat');
	}

	public function details_plagiat($plagiat_id='')
	{
		$this->load->model('plagiat');
		$plagiat = $this->plagiat->get($plagiat_id);
		if ($plagiat) {
			$this->api_url ='api/get_records_by_plagiat_id/'.$plagiat_id;
			$data['plagiat'] 	= $this->plagiat->get_join_by_plagiat_id($plagiat_id);
			$this->layout->set_template('mvp_template')
			->set_title('Details Plagiat - Scaniplag')
			->render_action_view($data);
		}else{
			redirect('dashboard/index','refresh');
		}
	}

	public function kirim_submit_tugas()
	{
		$this->load->model('plagiat');
		$this->load->model('plagiat_reports');

		if ($this->form_validation->run('kirim_submit_tugas')) {
			
			$sources_path 	= '';

			$config['upload_path']          = APPPATH.'submissions/';
			$config['allowed_types']        = 'zip';
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('files'))
			{
				$upload 	= $this->upload->data();
				$zipArchive = new ZipArchive();
				$result		= $zipArchive->open($upload['full_path']);
				if ($result === TRUE) {
					$folder_name 	= time();
					$sources_path 	= APPPATH.'extracts/';
					$sources_path  .= $folder_name; 
					if(!is_dir($sources_path)){
						mkdir($sources_path, 755, true);
					}
					$zipArchive ->extractTo($sources_path);
					$zipArchive ->close();

					$data 	= $this->input->post(NULL,true);
					$user_id = $this->ion_auth->get_user_id();
					$input 	= array(
						'course_id'=> $data['course_id'],
						'module_id'=> $data['module_id'],
						'language'=> $data['language'],
						'sources'=> $folder_name,
						'user_id'=> $user_id
					);
					if ($plagiat_id = $this->plagiat->insert($input)) {
						$files = get_files_in($sources_path);
						if ($files) {
							$n_files = count($files);
							$success = 0;
							foreach ($files as $index => $file) {
								$values = array(
									'plagiat_id'=> $plagiat_id,
									'file_name'=> $file
								);
								if ($this->plagiat_reports->insert($values)) {
									$success++;
								}
							}
							if ($n_files == $success) {
								$this->layout->set_alert('success','Sukses! semua tugas tersimpan');
							}else{
								$this->layout->set_alert('info',$success.' tugas tersimpan');
							}
							redirect('asisten/details_submit/'.$plagiat_id,'refresh');
						}else{
							$this->layout->set_alert('warning','File tidak ditemukan');
						}
					}else{
						$this->layout->set_alert('danger','Gagal menyimpan submit tugas');
					}
				}else{
					$this->layout->set_alert('warning','Zip gagal diextract');
				}
			}else{
				$this->layout->set_alert('warning','Upload sources code Gagal');
			}	
		}else{
			$this->layout->set_alert('warning',validation_errors());
		}
		go_back('submit_tugas');
	}

	public function details_submit($plagiat_id='')
	{
		$this->load->model('plagiat');
		$plagiat = $this->plagiat->get($plagiat_id);
		if ($plagiat) {
			$this->api_url ='api/get_report_by_plagiat_id/'.$plagiat_id;
			$data['plagiat'] 	= $this->plagiat->get_join_by_plagiat_id($plagiat_id);
			$this->layout->set_template('mvp_template')
			->set_title('Nilai Plagiat - Scaniplag')
			->render_action_view($data);
		}else{
			redirect('dashboard/index','refresh');
		}
		
	}

	public function praktikum()
	{
		$user_id = $this->ion_auth->get_user_id();
		$this->api_url ='api/get_praktikum_by/'.$user_id;
		$this->layout->set_template('mvp_template')
		->set_title('Daftar Praktikum - Scaniplag')
		->render_action_view();
	}

	public function plagiat()
	{
		$this->api_url ='api/get_plagiat_by_asisten';
		$this->layout->set_template('mvp_template')
		->set_title('Daftar Submit - Scaniplag')
		->render_action_view();
	}

	public function details_praktikum($course_id='')
	{
		$this->load->model('courses');
		$this->load->model('courses_assistants');
		$this->load->model('courses_modules');
		$course = $this->courses->get($course_id);
		
		if ($course) {
			$this->api_url 		= 'api/get_modules_by_course/'.$course_id;
			$data['course'] 	= $course;
			$data['modules'] 	= $this->courses_modules->get_by_course($course_id);
			$data['asisten'] 	= $this->courses_assistants->get_many_by('course_id',$course_id);
			$this->layout->set_template('mvp_template')
			->set_title('Details praktikum - Scaniplag')
			->render_action_view($data);
		}else{
			$this->layout->set_alert('danger','Praktikum tidak ada');
			redirect('asisten/praktikum','refresh');
		}
	}

	public function input_nilai($report_id='')
	{
		$this->load->helper('tools');

		$this->load->model('plagiat_reports');
		$this->load->model('plagiat_records');
		$report = $this->plagiat_reports->get($report_id);
		if ($report) {
			
			$filename 	= pathinfo($report->file_name, PATHINFO_FILENAME);
			$records 	= $this->plagiat_records->get_records_by_filename($filename);
			$sys_score 	= array();
			$n_rows 	= count($records);
			if ($records) {
				foreach ($records as $key => $value) {
					$sys_score[] = $value->score;
				}
			}	
			$amount_score 	= array_sum($sys_score) ;
			$amount_all 	= $n_rows * 100;
			$plagiat_score 	= ($amount_score/$amount_all) * 100;
			$data['report'] 		= $report;
			$data['records'] 		= $records;
			$data['plagiat_score']  = $plagiat_score;
			$this->layout->set_template('mvp_template')
			->set_title('Input Nilai - Scaniplag')
			->render_action_view($data);
		}else{
			$this->layout->set_alert('danger','report tidak ada');
			go_back('plagiat');
		}
	}

	public function download_source_codes_praktikan($report_id='')
	{
		$this->load->model('plagiat');
		$this->load->model('plagiat_reports');
		$report = $this->plagiat_reports->get($report_id);
		if ($report) {
			$plagiat  = $this->plagiat->get($report->plagiat_id);
			$filename = APPPATH.'extracts/'.$plagiat->sources.'/'.$report->file_name;
			push_to_download($filename);
		}else{
			$this->layout->set_alert('danger','source code tidak ada');
			go_back('plagiat');
		}
	}

	public function submit_input_nilai($report_id='')
	{
		$this->load->model('plagiat_reports');
		$report = $this->plagiat_reports->get($report_id);
		if ($report) {
			if ($this->form_validation->run('submit_input_nilai')) {
				$data = $this->input->post(NULL,true);
				if ($this->plagiat_reports->update($report_id,$data)) {
					$this->layout->set_alert('success','Nilai berhasil tersimpan');
					redirect('asisten/details_submit/'.$report->plagiat_id,'refresh');
				}else{
					$this->layout->set_alert('warning','Nilai gagal tersimpan');
				}
			}else{
				$this->layout->set_alert('warning',validation_errors());
			}
		}else{
			$this->layout->set_alert('danger','source code tidak ada');
		}
		go_back('input_nilai/'.$report_id);
	}
}