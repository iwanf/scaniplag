<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Admin extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();		
		if (!$this->ion_auth->is_admin()) {
			redirect('dashboard/index','refresh');
		}
	}

	public function index(){
		redirect('dashboard/index','refresh');
	}

	public function asisten()
	{
		$this->api_url = 'api/get_asisten';
		$this->layout->set_template('mvp_template')
		->set_title('Daftar Asisten - Scaniplag')
		->render_action_view();
	}

	public function praktikum()
	{
		$this->api_url = 'api/get_praktikum';
		$this->layout->set_template('mvp_template')
		->set_title('Daftar Praktikum - Scaniplag')
		->render_action_view();
	}

	public function details_asisten($user_id=''){
		$this->load->model('users');
		$this->load->model('courses');
		$this->load->model('courses_assistants');

		$this->api_url ='api/get_praktikum_by/'.$user_id;

		$data['asisten'] 	= $this->users->get($user_id);

		$exceptions = $this->courses_assistants->get_available_by_asisten($user_id);
		if ($exceptions) {
			$data['available'] 	= $exceptions;
		}else{
			$data['available'] 	= $this->courses->as_object()->get_all();
		}
		
		$this->layout->set_template('mvp_template')
		->set_title('Details Asisten - Scaniplag')
		->render_action_view($data);
	}

	public function tambah_asisten()
	{
		$this->layout->set_template('mvp_template')
		->set_title('Tambah Asisten - Scaniplag')
		->render_action_view();
	}

	public function submit_tambah_asisten()
	{
		$this->load->model('ion_auth_model');
		$this->load->model('users');

		if ($this->form_validation->run('submit_tambah_asisten')) {
			$data 		= $this->input->post(null,true);
			
			$additional_data['first_name'] 	= $data['first_name'];
			$additional_data['last_name'] 	= $data['last_name'];
			$additional_data['phone'] 		= $data['phone'];

			$username 		= $data['first_name'];
			$len_lastname 	= strlen($data['last_name']);
			

			if ($len_lastname > 1) {
				$username 	.= substr($data['last_name'], 0,2);
			}

			$last_id 	= $this->users->get_last_id();
			$username 	.= $last_id;

			$user_id 	= $this->ion_auth_model->register( 
				$username
				, $data['password']
				, $data['email']
				, $additional_data	
				, array(2)
			);
			if($user_id){
				$this->db->where('id',$user_id);
				$this->db->update('users',array('active'=>1));
				$this->layout->set_alert('success','Asisten berhasil ditambahkan');
				redirect('admin/asisten','refresh');
			}else{
				$this->layout->set_alert('warning',$this->ion_auth->messages());
			}	
		}else{
			$this->layout->set_alert('warning',validation_errors());
		}
		go_back('tambah_asisten');
	}

	public function input_praktikum($user_id)
	{
		$this->load->model('users');
		$this->load->model('courses_assistants');

		$user = $this->users->get($user_id);
		if ($user) {
			$data = $this->input->post(null,true);
			if (isset($data['course_id']) && $data['course_id']) {
				foreach ($data['course_id'] as $key => $value) {
					$send = array(
						'user_id' => $user_id,
						'course_id' => $value,
					);

					$this->courses_assistants->insert($send);
				}
			}
			$this->layout->set_alert('success','Berhasil menambahkan praktikum');

			redirect('admin/details_asisten/'.$user_id,'refresh');
		}else{
			go_back('asisten');
		}
	}

	public function edit_praktikum($course_id='')
	{
		$this->load->model('courses');
		$this->load->model('ion_auth_model');
		$data['dosen'] = $this->ion_auth_model->users(array(3))->result();
		$data['course'] = $this->courses->as_object()->get($course_id);
		$this->layout->set_template('mvp_template')
		->set_title('Edit praktikum - Scaniplag')
		->render_action_view($data);
	}

	public function details_praktikum($course_id='')
	{
		$this->load->model('courses');
		$this->load->model('courses_assistants');
		$course = $this->courses->get($course_id);
		if ($course) {
			$this->api_url 		= 'api/get_modules_by_course/'.$course_id;
			$data['course'] 	= $course;
			$data['asisten'] 	= $this->courses_assistants->get_many_by('course_id',$course_id);
			$this->layout->set_template('mvp_template')
			->set_title('Details praktikum - Scaniplag')
			->render_action_view($data);
		}else{
			$this->layout->set_alert('danger','Praktikum tidak ada');
			redirect('admin/praktikum','refresh');
		}
		
	}

	public function tambah_praktikum()
	{
		$this->load->model('ion_auth_model');
		$data['dosen'] = $this->ion_auth_model->users(array(3))->result();
		$this->layout->set_template('mvp_template')
		->set_title('Tambah Praktikum - Scaniplag')
		->render_action_view($data);
	}

	public function details_modul($id=''){
		$this->load->model('courses');
		$this->load->model('courses_modules');
		$module = $this->courses_modules->as_object()->get($id);
		if ($module) {
			$data['course'] 	= $this->courses->as_object()->get($module->course_id);
			$data['module'] 	= $module;
			$this->layout->set_template('mvp_template')
			->set_title('Details Modul - Scaniplag')
			->render_action_view($data);
		}else{
			$this->layout->set_alert('danger','Modul tidak ada');
			redirect('dashboard/index','refresh');
		}
	}

	public function edit_modul($id=''){
		$this->load->model('courses');
		$this->load->model('courses_modules');
		$module = $this->courses_modules->as_object()->get($id);
		if ($module) {
			$data['course'] 	= $this->courses->as_object()->get($module->course_id);
			$data['module'] 	= $module;
			$this->layout->set_template('mvp_template')
			->set_title('Edit Modul - Scaniplag')
			->render_action_view($data);
		}else{
			$this->layout->set_alert('danger','Modul tidak ada');
			redirect('dashboard/index','refresh');
		}
	}

	public function tambah_modul($course_id=''){
		$this->load->model('courses_modules');
		$this->load->model('courses');

		$course = $this->courses->get($course_id);
		if ($course) {

			if ($this->form_validation->run('tambah_modul')) {
				$config['upload_path']          = './modules/';
				$config['allowed_types']        = 'pdf';

				$this->load->library('upload', $config);
				if ($this->upload->do_upload('attachment'))
				{
					$upload = $this->upload->data();
					$input 	= $this->input->post(NULL,true);
					$series = array(
						'course_id' => $course_id,
						'title' => $input['title'],
						'summary' => $input['summary'],
						'attachment' => $upload['file_name']
					);

					if ($this->courses_modules->insert($series)) {
						$this->layout->set_alert('success','Sukses menyimpan modul');
						redirect('admin/details_praktikum/'.$course_id,'refresh');
					}else{
						$this->layout->set_alert('warning','Gagal menyimpan modul');
					}
				}else{
					$this->layout->set_alert('warning','Gagal upload materi');
				}
			}else{
				$this->layout->set_alert('warning',validation_errors());
			}
		}else{
			$this->layout->set_alert('danger','Praktikum tidak ada');
		}
		go_back('details_praktikum/'.$course_id);
	}

	public function submit_tambah_praktikum()
	{
		$this->load->model('courses');
		if ($this->form_validation->run('submit_tambah_praktikum')) {
			$data = $this->input->post(NULL,true);
			if ($praktikum_id = $this->courses->insert($data)) {
				$this->layout->set_alert('success','Sukses menyimpan praktikum');
				redirect('admin/details_praktikum/'.$praktikum_id,'refresh');
			}else{
				$this->layout->set_alert('warning','Gagal menyimpan praktikum');
			}
		}else{
			$this->layout->set_alert('warning',validation_errors());
		}
		go_back('tambah_praktikum');
	}

	public function submit_edit_praktikum($course_id='')
	{
		$this->load->model('courses');
		$course = $this->courses->get($course_id);
		if ($course) {
			if ($this->form_validation->run('submit_edit_praktikum')) {
				$data = $this->input->post(NULL,true);
				if ($praktikum_id = $this->courses->update($course_id,$data)) {
					$this->layout->set_alert('success','Sukses update praktikum');
					redirect('admin/details_praktikum/'.$praktikum_id,'refresh');
				}else{
					$this->layout->set_alert('warning','Gagal update praktikum');
				}
			}else{
				$this->layout->set_alert('warning',validation_errors());
			}
		}else{
			$this->layout->set_alert('danger','Praktikum tidak ada');
		}
		go_back('tambah_praktikum');
	}

	public function submit_edit_modul($id='')
	{
		$this->load->model('courses');
		$this->load->model('courses_modules');
		$module = $this->courses_modules->as_object()->get($id);
		if ($module) {

			if ($this->form_validation->run('tambah_modul')) {
				$config['upload_path']          = './modules/';
				$config['allowed_types']        = 'pdf';

				$this->load->library('upload', $config);
				$upload = array();
				if ($this->upload->do_upload('attachment'))
				{
					unlink('./modules/'.$module->attachment);

					$upload = $this->upload->data();
					
				}

				$input 	= $this->input->post(NULL,true);
				$series['title'] 	= $input['title'];
				$series['summary'] 	= $input['summary'];
				if ($upload && isset($upload['file_name'])) {
					$series['attachment'] 	= $upload['file_name'];
				}

				if ($this->courses_modules->update($id,$series)) {
					$this->layout->set_alert('success','Sukses update modul');
				}else{
					$this->layout->set_alert('warning','Gagal update modul');
				}
			}else{
				$this->layout->set_alert('warning',validation_errors());
			}

			redirect('admin/details_praktikum/'.$module->course_id,'refresh');
		}else{
			$this->layout->set_alert('danger','Modul tidak ada');
			redirect('dashboard/index','refresh');
		}
	}

	public function dosen()
	{
		$this->api_url = 'api/get_dosen';
		$this->layout->set_template('mvp_template')
		->set_title('Daftar - Dosen')
		->render_action_view();
	}

	public function import_add_dosen()
	{
		$this->load->library('excel');
		
		if (isset($_FILES['filexlsx'])) {
			if (
				$_FILES['filexlsx']['error'] == UPLOAD_ERR_OK && 
				is_uploaded_file($_FILES['filexlsx']['tmp_name'])
			) { 
				$this->excel->read($_FILES['filexlsx']['tmp_name'])->insert_to_dosen();
			}else{
				$message = 'Wajib melampirkan berkas xlsx yang akan diimport ';
				$this->session->set_flashdata('message',$message);
			}
		}else{
			$this->session->set_flashdata('message','tidak ada input lampiran file xlsx ');
		}
		go_back('dosen');
	}

	public function export_dosen()
	{
		$this->load->library('excel');
		$this->load->model('ion_auth_model');

		$fields 	= array('id','nik','nidn');
		$file_name	='Dosen Praktikum - ITTelkom Purwokerto';
		$sheet_name ='users';
		$heading 	= array(
			'first_name',
			'last_name',
			'email',
			'phone',
		);
		$sources 	= array();
		$data 		= $this->ion_auth_model->users(array(3));
		
		if ($this->excel->export_to_xlsx($file_name,$sheet_name,$heading,$data)) {
			$file  = FCPATH.'Dosen Praktikum - ITTelkom Purwokerto.xlsx';
			push_to_download($file);
		}else{
			$this->session->set_flashdata('message','format tidak tersedia');
			redirect('admin/dosen','refresh');
		}
	}
}