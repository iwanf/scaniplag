<?php

$config['prefix_method'] = array('index','add_','edit_','archive_','remove_','buat_','lihat_','tutup_','buka_','checklist_');
/**
* please using font-awesome to insert icon 
*/
$config['icon']['index_pengaduan_logistik'] ='fa-truck';
$config['icon']['index_pengaduan_keuangan'] ='fa-money';
$config['icon']['index_pengaduan_akademik'] ='fa-graduation-cap';
$config['icon']['index_staff'] 				='fa-user-md';
$config['icon']['index_mahasiswa'] 			='fa-users';