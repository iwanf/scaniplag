<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'simpan_permission'=>array(
		array('field'=>'group_id','label'=>'Level User','rules'=>'trim|required'),
		array('field'=>'modul','label'=>'Module aplikasi','rules'=>'trim|required')
	),
	'login'=>array(
		array('field'=>'identity','label'=>'Email','rules'=>'trim|required'),
		array('field'=>'password','label'=>'Password','rules'=>'trim|required')
	),
	'register'=>array(
		array('field'=>'first_name','label'=>'First Name','rules'=>'trim|required'),
		//array('field'=>'last_name','label'=>'Last Name','rules'=>'trim|required'),
		array('field'=>'identity','label'=>'Identity','rules'=>'trim|required'),
		array('field'=>'email','label'=>'Email','rules'=>'trim|required|valid_email|is_unique[users.email]'),
		array('field'=>'phone','label'=>'Phone','rules'=>'trim|required'),
		array('field'=>'password','label'=>'Password','rules'=>'trim|required'),
		array('field'=>'password_confirm','label'=>'Confirm Password','rules'=>'trim|required|matches[password]')
	),
	'submit_tambah_modul'=>array(
		array('field'=>'title','label'=>'Judul','rules'=>'trim|required|is_unique[courses_modules.title]')
	),
	'submit_tambah_kelas'=>array(
		array('field'=>'class_name','label'=>'Nama Kelas','rules'=>'trim|required|is_unique[students_classes.class_name]')
	),
	'submit_praktikum'=>array(
		array('field'=>'language','label'=>'Bahasa Pemograman ','rules'=>'trim|required'),
		array('field'=>'course_id','label'=>'Praktikum','rules'=>'trim|required'),
		array('field'=>'module_id','label'=>'Modul','rules'=>'trim|required'),
		array('field'=>'mml','label'=>'Tingkat Kemiripan','rules'=>'trim|required'),
	),
	'submit_tambah_asisten'=>array(
		array('field'=>'email','label'=>'Email','rules'=>'trim|required|valid_email|is_unique[users.email]'),
		array('field'=>'password','label'=>'Password','rules'=>'trim|required'),
		array('field'=>'password_confirm','label'=>'Confirm Password','rules'=>'trim|required|matches[password]')
	),
	'submit_tambah_praktikum'=>array(
		array('field'=>'course_name','label'=>'Praktikum','rules'=>'trim|required|is_unique[courses.course_name]'),
		array('field'=>'lecturer_id','label'=>'Dosen Pengampu','rules'=>'trim|required'),
		array('field'=>'amount_modules','label'=>'Jml. Modul','rules'=>'trim|required')
	),
	'submit_edit_praktikum'=>array(
		array('field'=>'course_name','label'=>'Praktikum','rules'=>'trim|required'),
		array('field'=>'lecturer_id','label'=>'Dosen Pengampu','rules'=>'trim|required'),
		array('field'=>'amount_modules','label'=>'Jml. Modul','rules'=>'trim|required')
	),
	'tambah_modul'=>array(
		array('field'=>'title','label'=>'Judul','rules'=>'trim|required')
	),
	'kirim_submit_tugas'=>array(
		array('field'=>'language','label'=>'Bahasa Pemograman ','rules'=>'trim|required'),
		array('field'=>'course_id','label'=>'Praktikum','rules'=>'trim|required'),
		array('field'=>'module_id','label'=>'Modul','rules'=>'trim|required'),
	),
	'submit_cek_plagiat'=>array(
		array('field'=>'language','label'=>'Bahasa Pemograman ','rules'=>'trim|required'),
		array('field'=>'mml','label'=>'Tingkat Kemiripan','rules'=>'trim|required'),
		array('field'=>'sources','label'=>'Sources','rules'=>'trim|required'),
	),
	'submit_input_nilai'=>array(
		array('field'=>'assistant_score','label'=>'Nilai Asisten','rules'=>'trim|required'),
	),
	
);