<?php

/**
 * 
 */
class Students extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_unregistered()
	{
		$this->fields = array(
			'nim',
			'year_period',
			'current_semester'
		);
		$this->_database->join('users','users.student_id = students.id','left');
		$this->_database->where('users.student_id IS NULL',null);
		return $this->get_datatables_format();
	}
}