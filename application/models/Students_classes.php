<?php

/**
 * 
 */
class Students_classes extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_aktif()
	{
		return $this->get_many_by('archived','false');
	}
	
}