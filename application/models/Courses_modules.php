<?php

/**
 * 
 */
class Courses_modules extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_by_course($course_id='')
	{
		return $this->get_many_by('course_id',$course_id);
	}
}