<?php
/**
 * 
 */
class Courses extends MY_Model
{
	public $return_type 	= 'array';
	//public $before_create 	= array('update_lecturer_id');

	function __construct()
	{
		parent::__construct();
	}

	public function get_by_lecturer($lecturer_id='',$options=array())
	{
		$this->_database->select(array(
			'id',
			'course_code',
			'course_name',
			'pretest_date',
			'postest_date'
		));
		$this->_database->where('lecturer_id',$lecturer_id);
		return $this->get_datatables_format($options);
	}

	public function update_lecturer_id($courses)
	{
		
		$this->load->library('ion_auth');
		$user_id 	= $this->ion_auth->get_user_id();
		if ($user_id) {
			$this->db->where('id',$user_id);
			$user 					= $this->db->get('users')->row();
			$courses['lecturer_id'] = $user->lecturer_id;
		}
		return $courses;
	}

	public function availables()
	{
		$sql ='SELECT `courses`.* FROM `courses` LEFT JOIN courses_assistants ON courses_assistants.course_id = courses.id WHERE courses_assistants.course_id IS NULL';
		$query = $this->db->query($sql);
		return $query->result();
	}

}