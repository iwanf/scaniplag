<?php

/**
 * 
 */
class Plagiat_records extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_by_plagiat_id($plagiat_id)
	{
		return $this->get_many_by('plagiat_id',$plagiat_id);
	}
	
	public function get_records_by_filename($filename)
	{
		$this->db->like('master',"$filename");
		$this->db->or_like('comparer',"-$filename");
		return $this->db->get('plagiat_records')->result();
	}
}