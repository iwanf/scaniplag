<?php

/**
 * 
 */
class Plagiat extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function count_un_checked_by($user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('mml IS NULL',NULL);
		$amount = $this->db->get('plagiat')->num_rows();
		return $amount;
	}

	public function get_un_checked_by($user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('mml IS NULL',NULL);
		$data = $this->db->get('plagiat')->result();
		return $data;
	}

	public function get_join_by_user_id($user_id)
	{
		$this->db->select(array(
			'a.*',
			'b.course_name',
			'c.title'
		));
		$this->db->from('plagiat a');
		$this->db->where('a.`user_id`',$user_id);
		$this->db->join('courses b', 'a.`course_id` = b.`id`', 'inner');
		$this->db->join('courses_modules c', 'a.`module_id` = c.`id`', 'inner');
		return $this->db->get()->result();
	}

	public function get_join_by_plagiat_id($plagiat_id)
	{
		$this->db->select(array(
			'a.*',
			'b.course_name',
			'c.title'
		));
		$this->db->from('plagiat a');
		$this->db->where('a.`id`',$plagiat_id);
		$this->db->join('courses b', 'a.`course_id` = b.`id`', 'inner');
		$this->db->join('courses_modules c', 'a.`module_id` = c.`id`', 'inner');
		return $this->db->get()->row();
	}
}