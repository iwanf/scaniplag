<?php

/**
 * 
 */
class Courses_has_classes extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_class_by_course($course_id='')
	{
		$this->db->select(array(
			'courses_has_classes.class_id ',
			'students_classes.class_name',
			'students_classes.amount_students',
		));
		$this->db->from('courses_has_classes');
		$this->db->where('course_id',$course_id);
		$this->db->join('students_classes','courses_has_classes.class_id =  students_classes.id');
		$this->db->join('courses','courses_has_classes.course_id =  courses.id');
		return $this->db->get()->result();
	}
}