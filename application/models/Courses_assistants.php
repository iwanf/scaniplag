<?php

/**
 * 
 */
class Courses_assistants extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function get_by_asisten($user_id)
	{
		$data 		= $this->get_many_by('user_id',$user_id);
		$courses_id = array();
		if ($data) {
			foreach ($data as $key => $value) {
				$courses_id[] = $value->course_id;
			}
		}
		if ($courses_id) {
			$this->db->where_in('id',$courses_id);
			return $this->db->get('courses')->result();
		}else{
			return $courses_id;
		}	
	}

	public function get_available_by_asisten($user_id='')
	{
		$data = $this->get_by_asisten($user_id);
		$send 		= array();
		if ($data) {
			$courses_id = array();
			foreach ($data as $key => $value) {
				$courses_id[] = $value->id;
			}
			if ($courses_id) {
				$send 	= $this->db->where_not_in('id',$courses_id)->get('courses')->result();
			}
		}
		return $send;
	}
}