<?php

/**
 * 
 */
class Lecturers extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_unregistered()
	{
		$this->fields = array(
			'nik',
			'nidn'
		);
		$this->_database->join('users','users.student_id = lecturers.id','left');
		$this->_database->where('users.lecturer_id IS NULL',null);
		return $this->get_datatables_format();
	}
}