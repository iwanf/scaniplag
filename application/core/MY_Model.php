<?php

/**
* 
*/
require 'Jamie_Model.php';
require 'ssp.class.php';
class MY_Model extends Jamie_Model
{

	public $fields;
	protected $primary_key;

	function __construct()	
	{
		parent::__construct();
		$this->load->config('ion_auth');
		
		$this->fields = $this->db->list_fields($this->_table);
		$this->primary_key = $this->get_primary_key();
	}

	public function get_enum_values($field)
	{
		$sql = "SHOW COLUMNS FROM ".$this->db->dbprefix."{$this->_table} WHERE Field = '{$field}'";
		$type = $this->db->query($sql)->row(0)->Type; 
		preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
		$enum = explode("','", $matches[1]);
		return $enum; 	  	
	}

	protected function get_primary_key(){
		$query 	= "SHOW KEYS FROM ".$this->_table." WHERE Key_name = 'PRIMARY'";
		$result = $this->db->query($query)->row();
		if ($result) {
			return $result->Column_name;
		}else{
			return false;
		}
	}

	public function get_datatables_server_side_format($columns=array())
	{
		$table 		= $this->_table;
		$primaryKey = $this->primary_key;
		if (!$columns && $this->fields) {
			
			$sensitive_fields = $this->config->item('sensitive_fields');
			foreach ($this->fields as $key => $value) {
				if (!in_array($value, $sensitive_fields)) {
					$columns[] = array(
						'db'=>$value,
						'dt'=> $key
					);
				}
			}
		}

		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname,
		);
		return SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);
	}

	public function get_datatables_format($options=array())
	{
		$rows 	= array();
		$data 	= array();
		
		$rows	= $this->get_all();

		if ($rows) {
			$number = 1;
			foreach ($rows as $key => $value) {
				$action = '';

				if ($options) {
					foreach ($options as $label => $opsi) {
						$field_key 	= $this->primary_key;
						$opsi['url'] .='/'.$value[$field_key];

						if ($opsi['type'] == 'danger') {
							$action .= anchor(
								$opsi['url'],
								$label,
								array(
									'class'=>'btn-confirm uk-button uk-button-small uk-button-'.$opsi['type'],
								)
							);
						}else{
							$action .= anchor(
								$opsi['url'],
								$label,
								array('class'=>'uk-button uk-button-small uk-button-'.$opsi['type'])
							);
						}
						
					}
				}
				
				$catch = array();

				array_push($catch, $number);
				foreach ($value as $index => $item) {
					if (in_array($index, $this->fields)) {
						$catch[] = $item;
					}

				}
				array_push($catch, $action);
				$data[] = $catch;
				$number++;
			}
		}

		$json['data'] = $data;
		return $json;
	}


	function get_last_id()
	{
		$sql 	= 'SELECT MAX(id)+1 as `id` FROM '.$this->_table.'';
		$result = $this->db->query($sql)->row();
		return ($result)? $result->id : 1;
	}
}