<?php


function get_info_user($user_id='',$field='username')
{
	$ci =&get_instance();
	$ci->load->model('users');

	if ($user = $ci->users->get($user_id)) {
		if (isset($user[$field])) {
			return ucwords($user[$field]);
		}
	}
}

function get_info_group($group_id='',$field='name')
{
	$ci =&get_instance();
	$ci->load->model('groups');

	if ($groups = $ci->groups->get($group_id)) {
		if (isset($groups[$field])) {
			return ucwords($groups[$field]);
		}
	}
}

function get_admin_menu()
{
	$menu = array(
		array(
			'icon'=>'home',
			'url'=>'admin/index',
			'label'=>'Beranda'
		),
		array(
			'icon'=>'play',
			'url'=>'admin/pindai_plagiat',
			'label'=>'Pindai Plagiat'
		),
		array(
			'icon'=>'users',
			'url'=>'admin/pengguna',
			'label'=>'Pengguna',
			'sub'=> array(
				array('icon'=>'users','url'=>'admin/pengguna','label'=>'semua'),
				array('icon'=>'happy','url'=>'admin/mahasiswa','label'=>'mahasiswa'),
				array('icon'=>'user','url'=>'admin/dosen','label'=>'dosen')
			)
		)
	);
	return $menu;
}

function get_mahasiswa_menu()
{
	$menu = array(
		array(
			'icon'=>'home',
			'url'=>'dashboard/index',
			'label'=>'Beranda'
		),
		array(
			'icon'=>'bell',
			'url'=>'mahasiswa/tugas',
			'label'=>'Tugas'
		),
		array(
			'icon'=>'bookmark',
			'url'=>'mahasiswa/matakuliah',
			'label'=>'Matakuliah'
		),
		array(
			'url'=>'mahasiswa/ujian',
			'label'=>'Ujian',
			'sub'=> array(
				array('icon'=>'list','url'=>'mahasiswa/ujian','label'=>'semua'),
				array('icon'=>'history','url'=>'mahasiswa/pretest','label'=>'pretest'),
				array('icon'=>'future','url'=>'mahasiswa/postest','label'=>'postest')
			)
		)
	);
	return $menu;
}

function get_dosen_menu()
{
	$menu = array(
		array(
			'icon'=>'home',
			'url'=>'dashboard/index',
			'label'=>'Beranda'
		),
		array(
			'icon'=>'bookmark',
			'url'=>'dosen/matakuliah',
			'label'=>'Matakuliah'
		),
		array(
			'icon'=>'happy',
			'url'=>'dosen/hasil_ujian',
			'label'=>'Hasil Ujian'
		),
		array(
			'url'=>'dosen/ujian',
			'label'=>'Ujian',
			'sub'=> array(
				array('icon'=>'list','url'=>'dosen/ujian','label'=>'semua'),
				array('icon'=>'history','url'=>'dosen/pretest','label'=>'pretest'),
				array('icon'=>'future','url'=>'dosen/postest','label'=>'postest')
			)
		)
	);
	return $menu;
}

function mvp_menu_admin()
{
	$menu = array(
		array('icon'=>'home','url'=>'dashboard/index','label'=>'Beranda'),
		array('icon'=>'user','url'=>'admin/dosen','label'=>'Dosen Praktikum'),
		array('icon'=>'laptop','url'=>'admin/praktikum','label'=>'Praktikum'),
		array('icon'=>'happy','url'=>'admin/asisten','label'=>'Asisten'),
	);
	return $menu;
}

function mvp_menu_asisten()
{
	$menu = array(
		array('icon'=>'home','url'=>'dashboard/index','label'=>'Beranda'),
		array('icon'=>'laptop','url'=>'asisten/praktikum','label'=>'Praktikum'),
		array('icon'=>'tag','url'=>'asisten/plagiat','label'=>'Plagiat'),
	);
	return $menu;
}

function go_back($page='index')
{
	$ci =&get_instance();
	if (isset($_SERVER['HTTP_REFERER'])) {
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		exit;
	}else{
		$controller = $ci->router->fetch_class();
		redirect($controller.'/'.$page,'refresh');
	}
}

function get_support_languages($index){
	$languages = array(
		'java15', //0
		'java17', //1
		'java15dm', //2
		'java12', //3
		'java11', //4
		'python3', //5
		'c/c++', //6
		'c#-1.2', //7
		'char', //8
		'text', //9
		'scheme' //10
	);
	return (isset($languages[$index]))? $languages[$index] : $languages;
}

function get_plagiarm_quotes(){
	$data = array(
		array(
			'text'=>"If you steal from one author it's plagiarism; if you steal from many it's research.",
			'author'=>"Wilson Mizner"
		),
		array(
			'text'=>"Art is either plagiarism or revolution.",
			'author'=>"Paul Gauguin"
		),
		array(
			'text'=>"Taking something from one man and making it worse is plagiarism.",
			'author'=>"George A. Moore"
		),
		array(
			'text'=>"Originality is undetected plagiarism. ",
			'author'=>"William Ralph Inge"
		),
		array(
			'text'=>"Certainly the plagiarism, and dealing with the fallout of it, was the most difficult thing I've ever faced since I started writing. ",
			'author'=>"Nora Roberts"
		),
		array(
			'text'=>"In many senses, creativity and 'plagiarism' are nearly indivisible.",
			'author'=>"David Shields"
		),
		array(
			'text'=>"Scientific fraud, plagiarism, and ghost writing are increasingly being reported in the news media, creating the impression that misconduct has become a widespread and omnipresent evil in scientific research.",
			'author'=>"Heinrich Rohrer"
		),
	);
	return $data;
}

function run_jplag($path='',$lang='java17')
{
	$source		= 'application/extracts/'.$path;
	$command 	= "java -jar jplag.jar -l $lang -r $result -s $source";
	$result 	= exec($command);
	return  $result;
}

function get_output_jplag_cli($out=array())
{
	$records 	= array();
	foreach ($out as $index => $line) {
		if (stripos(strtolower($line), 'comparing') !== false) {
			$firt_step 		= explode(':', $line);
			$score 			= $firt_step[1];
			$second_step 	= explode('.', $firt_step[0]);
			$last_index 	= count($second_step) -1;
			$extension 		= $second_step[$last_index]; // .cpp 
			$third_step 	= explode($extension, $firt_step[0]);
			$comparer 		= $third_step[1];
			$fourth_step 	= explode(" ", $third_step[0]);
			$master 		= $fourth_step[1];
			$records[] 		= array(
				'master' => strtolower($master),
				'comparer' => strtolower($comparer),
				'score' => floatval($score),
			);
		}
	}
	return $records;
}