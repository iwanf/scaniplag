<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<!-- panel -->
	<div class="uk-width-2-3@l uk-width-1-2@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-body">
				<form action="<?= site_url('dashboard/update_profile') ?>" method="post">
					<ul uk-tab="" class="uk-tab">
						<li aria-expanded="true" class="uk-active"><a href="#">Akun</a></li>
						<li aria-expanded="false"><a href="#">Biodata</a></li>
						<li aria-expanded="false"><a href="#">Password</a></li>
					</ul>
					
					<ul class="uk-switcher uk-margin">
						<li>
							
							<div class="uk-margin">
								<input class="uk-input" type="text" name="username" value="<?= (isset($profile['username']))? ucwords($profile['username']) : ''  ?>" placeholder="Username">
							</div>
							<div class="uk-margin">
								<input type="text" name="email" class="uk-input" value="<?= (isset($profile['email']))? $profile['email'] : ''  ?>" placeholder="Email">
							</div>
							<div class="uk-margin">
								<input type="text" name="phone" class="uk-input" value="<?= (isset($profile['phone']))? $profile['phone'] : ''  ?>" placeholder="Nomor Handphone">
							</div>
							<div class="uk-float-right">
								<button type="submit" class="uk-button uk-button-primary" >
									<i data-uk-icon="icon: check"></i>
									Simpan
								</button>
							</div>
							
						</li>
						<li>
							
							<div class="uk-margin">
								<input type="text" name="first_name" class="uk-input" value="<?= (isset($profile['first_name']))? $profile['first_name'] : ''  ?>" placeholder="Nama Depan">
							</div>
							<div class="uk-margin">
								<input type="text" name="last_name" class="uk-input" value="<?= (isset($profile['last_name']))? $profile['last_name'] : ''  ?>" placeholder="Nama Belakang">
							</div>
							<div class="uk-float-right">
								<button type="submit" class="uk-button uk-button-primary" >
									<i data-uk-icon="icon: check"></i>
									Simpan
								</button>
							</div>
							
						</li>
						<li>
							<div class="uk-margin">
								<label>Password baru</label>
								<input type="password" name="password" class="uk-input" >
							</div>
							<div class="uk-margin">
								<label>Konfirmasi Password baru</label>
								<input type="password" name="c_password" class="uk-input">
							</div>
							<div class="uk-float-right">
								<button type="submit" class="uk-button uk-button-primary" >
									<i data-uk-icon="icon: check"></i>
									Simpan
								</button>
							</div>
							
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>
	<!-- /panel -->
	<!-- panel -->
	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<div class="uk-card uk-card-default uk-card-small uk-card-hover">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h4 class="uk-margin-remove-bottom">
							Photo Profile
						</h4>
					</div>
					<div class="uk-width-expand uk-text-right">
						<a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: trash"></a>
						<a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: check"></a>
						<a href="#" class="uk-icon-link" data-uk-icon="icon: upload" title="Upload photo profile"></a>
					</div>
				</div>
			</div>
			<div class="uk-card-body">
				<img data-src="<?=base_url('public/images/placeholder-user.png') ?>" width="1800" height="1200" alt="" uk-img>
			</div>
		</div>
	</div>
	<!-- /panel -->
</div>