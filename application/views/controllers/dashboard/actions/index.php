<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
$CI =&get_instance(); 
?>
<header class="uk-text-center">
	<h1 class="uk-heading-primary"><?= $CI->config->item('app_name') ?></h1>
	<p class="uk-width-3-5 uk-margin-auto">
		<?= $CI->lang->line('scani_description') ?>
	</p>
</header>
<div class="uk-child-width-1-2@m" uk-grid>
	<?php
	if (isset($pintas)) {
		foreach ($pintas as $key => $value) { ?>
			<div>
				<div class="uk-text-center">
					<span class="uk-text-small"><span data-uk-icon="icon:<?= $value['icon'] ?>" class="uk-margin-small-right uk-text-primary"></span><?= $value['unit'] ?></span>
					<h1 class="uk-heading-primary uk-margin-remove  uk-text-primary">
						<?= $value['qty'] ?>
					</h1>
					<div class="uk-text-small">
						<a href="<?= $value['url'] ?>" class="uk-button uk-button-primary uk-width-1-2">
							<?= $value['label'] ?>
						</a>
					</div>
				</div>
			</div>
		<?php	}
	}
	?>

</div>


