<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li class="uk-disabled"><a>Profile</a> </li>
			</ul>
			<form action="<?= site_url('dashboard/update_profile') ?>" method="post">
				<ul uk-tab="" class="uk-tab">
					<li aria-expanded="true" class="uk-active"><a href="#">Akun</a></li>
					<li aria-expanded="false"><a href="#">Biodata</a></li>
					<li aria-expanded="false"><a href="#">Password</a></li>
				</ul>

				<ul class="uk-switcher uk-margin">
					<li>
						<div class="uk-margin">
							<strong>Username</strong>
							<input class="uk-input" type="text" name="username" value="<?= (isset($profile['username']))? ucwords($profile['username']) : ''  ?>" placeholder="Username">
						</div>
						<div class="uk-margin">
							<strong>Email</strong>
							<input type="text" name="email" class="uk-input" value="<?= (isset($profile['email']))? $profile['email'] : ''  ?>" placeholder="Email">
						</div>
						<div class="uk-margin">
							<strong>No. Handphone</strong>
							<input type="text" name="phone" class="uk-input" value="<?= (isset($profile['phone']))? $profile['phone'] : ''  ?>" placeholder="Nomor Handphone">
						</div>
						<div class="uk-float-right">
							<button type="submit" class="uk-button uk-button-primary" >
								<i data-uk-icon="icon: check"></i>
								Simpan
							</button>
						</div>

					</li>
					<li>

						<div class="uk-margin">
							<strong>Nama Depan</strong>
							<input type="text" name="first_name" class="uk-input" value="<?= (isset($profile['first_name']))? $profile['first_name'] : ''  ?>" placeholder="Nama Depan">
						</div>
						<div class="uk-margin">
							<strong>Nama Belakang</strong>
							<input type="text" name="last_name" class="uk-input" value="<?= (isset($profile['last_name']))? $profile['last_name'] : ''  ?>" placeholder="Nama Belakang">
						</div>
						<div class="uk-float-right">
							<button type="submit" class="uk-button uk-button-primary" >
								<i data-uk-icon="icon: check"></i>
								Simpan
							</button>
						</div>

					</li>
					<li>
						<div class="uk-margin">
							<strong>Password baru</strong>
							<input type="password" name="password" class="uk-input" >
						</div>
						<div class="uk-margin">
							<strong>Konfirmasi Password baru</strong>
							<input type="password" name="c_password" class="uk-input">
						</div>
						<div class="uk-float-right">
							<button type="submit" class="uk-button uk-button-primary" >
								<i data-uk-icon="icon: check"></i>
								Simpan
							</button>
						</div>

					</li>
				</ul>
			</form>
		</div>
	</div>
</div>