<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
$CI =&get_instance(); 
?>

<!--HERO-->
<section class="uk-section uk-section-large uk-section-secondary" style="background-image: url('<?= base_url("public/images/landing-bg.jpg") ?>'); height: 50%;background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;">
	<div class="uk-container uk-container-small uk-text-center" >
		<ul class="uk-subnav uk-tab uk-flex-center uk-hidden">
			<?php if(isset($quotes)) : ?>
			<?php foreach ($quotes as $key => $value) { ?>
			<li><a href="#">Item <?= $key ?></a></li>
			<?php } ?>
		<?php endif; ?>
	</ul>
	<ul class="uk-switcher uk-text-center">
		<?php if(isset($quotes)) : ?>
		<?php foreach ($quotes as $key => $value) { ?>
		<?= ($key == 0)? '<li class="uk-active">' : '<li>' ?>
		<i><h1 id="q-<?= $key ?>"><?= $value['text'] ?></h1></i>
		<p class="uk-text-large">- <?= $value['author'] ?> - </p>
	</li>
	<?php } ?>
<?php endif; ?>
</ul>
<div style="margin-top:10px;" data-uk-margin>
	<div class="uk-button uk-button-primary" id="btn-copy-quote" onclick="copyQuote(0)">
		<span data-uk-icon="icon:copy"></span>&nbsp;
		Salin
	</div>
	<!--<div class="uk-button uk-button-primary">
		Ayo Mulai!
	</div>-->
</div>
</div>
</section>


<!--/HERO-->
<!-- INFO -->
<section class="uk-section uk-section-default uk-box-shadow-small uk-section-xsmall">
	<div class="uk-container">
		<div class="uk-grid uk-grid-small uk-flex uk-flex-middle" data-uk-grid>
			<div class="uk-width-auto">
				<img src="https://unsplash.it/65/65/?random" alt="" class="uk-border-circle">
			</div>
			<div class="uk-width-expand">
				<h4 class="uk-margin-remove">Sistem Pendeteksi Plagiat <i>Source Codes</i> Menggunakan Tools Jplag</h4>
				<i class="uk-text-muted uk-text-small">Dikembangkan dengan PHP (CodeIgniter)</i>
			</div>
			<div class="uk-width-1-2 uk-width-1-5@m uk-visible@m">
				<span class="uk-text-primary" data-uk-icon="icon:clock; ratio: 0.8"></span><span class="uk-text-small uk-text-muted uk-text-bottom"> Time</span><br>
				<span class="uk-text-large uk-text-primary" id="scaniplag-clock">00 : 00 : 00</span>
			</div>
			<div class="uk-width-1-2 uk-width-1-5@m uk-visible@m">
				<span class="uk-text-success" data-uk-icon="icon:calendar; ratio: 0.8"></span><span class="uk-text-small uk-text-muted  uk-text-bottom"> Date</span><br>
				<span class="uk-text-large uk-text-success"><?= date('jS M Y') ?></span>
			</div>

			<div class="uk-width-auto@m uk-visible@m">
				<!--<a class="uk-button uk-button-default">Back</a>-->
			</div>
		</div>
	</div>
</section>
<!-- /INFO -->

