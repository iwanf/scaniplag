<div class="uk-margin-small-top" id="recover" >
    <?= form_open('welcome/submit_forgot_password') ?>

    <div class="uk-margin-small">
        <div class="uk-inline uk-width-1-1">
            <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
            <input name="identity" class="uk-input" placeholder="E-mail" required type="text">
        </div>
    </div>
    <div class="uk-margin-small">
        <button type="submit" class="uk-button uk-button-primary uk-button-primary uk-width-1-1">
            <?= lang('forgot_password_submit_btn') ?>
        </button>
    </div>
    <?= form_close() ?>
</div>
<div class="uk-text-left">
    <span data-uk-icon="icon: arrow-left"></span>
    <?= anchor('welcome/login','Kembali') ?>
</div>