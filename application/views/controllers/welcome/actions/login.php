<?= form_open('welcome/verify_login');?>
<fieldset class="uk-fieldset">
    <legend class="uk-legend">Login</legend>
    <div class="uk-margin">
        <div class="uk-inline uk-width-1-1">
            <!--<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>-->
            <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
            <?= form_input($identity) ?>
        </div>
    </div>
    <div class="uk-margin">
        <div class="uk-inline uk-width-1-1">
            <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
            <?= form_input($password)?>
        </div>
    </div>

    <div class="uk-margin">
        <label>
            <?= form_checkbox('remember', '1', FALSE, array('id'=>'remember','class'=>'uk-checkbox'));?>
            <?= lang('login_remember_label', 'remember')?>
        </label>
    </div>
    <div class="uk-margin">
        <?= form_submit('LOG IN', lang('login_submit_btn'),array('class'=>'uk-button uk-button-primary uk-button-primary uk-button-large uk-width-1-1')) ?>
    </div>
</fieldset>
<?= form_close() ?>
<div>
    <div class="uk-text-center">
        <?= anchor('welcome/forgot_password',lang('login_forgot_password')) ?>
    </div>
</div>