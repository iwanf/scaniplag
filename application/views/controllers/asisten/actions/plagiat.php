<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li class="uk-disabled"><a>Plagiat</a> </li>
			</ul>
			<h1 class="uk-text-center">Daftar Plagiat</h1>
			
			<hr/>
			<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Praktikum</th>
						<th>Modul</th>
						<th>Bahasa</th>
						<th>Kemiripan</th>
						<th>Sources</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
