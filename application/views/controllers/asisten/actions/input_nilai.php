<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li><?= anchor('asisten/plagiat','Plagiat') ?></li>
				<li class="uk-disabled"><a><?= (isset($report->file_name))? $report->file_name : '' ?></a> </li>
			</ul>
			<div class="uk-text-center">
				<h1>Form Penilaian Praktikan</h1>
			</div>
			<?php
			if (isset($report->id)) {
				echo form_open('asisten/submit_input_nilai/'.$report->id);
			}
			?>
			<div class="uk-grid-small" uk-grid>
				<div class="uk-width-3-4@s">
					<strong>File Praktikan</strong><br/>
					<?= (isset($report->file_name))? anchor('asisten/download_source_codes_praktikan/'.$report->id,$report->file_name) : '' ?>
				</div>
				<div class="uk-width-1-6@s">
					<strong>% Plagiat</strong><br/>
					<?= (isset($plagiat_score))? $plagiat_score : '' ?>
					<input type="hidden" name="system_score" value="<?= (isset($plagiat_score))? $plagiat_score : '0' ?>" />
				</div>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<div class="uk-margin">
						<strong class="uk-form-label">Nilai Asisten</strong>
						<div class="uk-form-controls">
							<input type="number" step="any" name="assistant_score" class="uk-input" value="<?= (isset($report->assistant_score))? $report->assistant_score : ''?>">
						</div>
					</div>
				</div>
				<div style="padding-top: 25px;">

					<div class="uk-text-right">
						<button type="submit" class="uk-button uk-button-primary uk-width-1-2">
							Submit
						</button>
					</div>
				</div>
			</div>
			<?= form_close() ?>			
		</div>
		<div class="uk-card uk-card-default">

			<div class="uk-card-header">
				<h4>Details Perbandingan Plagiat</h4>
			</div>

			<table class="uk-table uk-table-hover uk-table-divider">
				<thead>
					<tr>
						<th>No</th>
						<th>A</th>
						<th>B</th>						
						<th>Score</th>
					</tr>
				</thead>
				<tbody>
					<?php

					if (isset($records) && $records) {
						foreach ($records as $key => $value) { 
							$number = $key+1;
							?>
							<tr>
								<th><?= $number ?></th>
								<th><?= (isset($value->comparer))? $value->master : '' ?></th>
								<th><?= (isset($value->comparer))? clean_string($value->comparer) : '' ?></th>				
								<th><?= (isset($value->master))? $value->score.'&nbsp;%' : '' ?></th>
							</tr>
						<?php	}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

