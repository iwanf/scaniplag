<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			
			<h1 class="uk-text-center">Form Submit Tugas</h1>
			<?= form_open_multipart('asisten/kirim_submit_tugas',array('id'=>'form-submit-tugas')) ?>

			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Bahasa Pemograman</strong>
					<code>*Wajib</code>
				</label>
				<div class="uk-form-controls">
					<?php
					if (isset($languages)) {
						foreach ($languages as $key => $value) { ?>
							<input type="radio"  name="language" value="<?= $key ?>" />
							<?= $value ?>
							<?php	
						}
					}
					?>
				</div>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Praktikum</strong>
							<code>*Wajib</code>
							<select name="course_id" class="uk-input" data-url="<?= site_url('api/modules_praktikum') ?>">
								<option value="0">Pilih Matakuliah</option>
								<?php
								if (isset($courses)) {
									foreach ($courses as $key => $value) { ?>
										<option value="<?= $value->id ?>"><?= $value->course_name ?></option>
									<?php	}
								}
								?>
							</select>
						</label>
					</div>
				</div>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Modul</strong>
							<code>*Wajib</code>
							<select name="module_id" class="uk-input">
								<option value="0">Pilih Materi</option>
							</select>
						</label>
					</div>
				</div>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Upload Source Codes</strong><code> (*.zip)</code>
				</label>
				<input type="file" name="files" accept=".zip" placeholder="Lampirkan Zip Source Code" class="uk-input">
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<button class="uk-button uk-button-default uk-modal-close" id="btn-modal-close" type="button" hidden="true"></button>
				</div>
				<div>
					<div class="uk-text-right">
						<button class="uk-button uk-button-primary" type="submit">Submit</button>
					</div>
				</div>
			</div>

			<?= form_close() ?>
		</div>
	</div>
</div>

<div id="modal-loading" uk-modal>
	<div class="dl">
		<div class="dl__container">
			<div class="dl__corner--top"></div>
			<div class="dl__corner--bottom"></div>
		</div>
		<div class="dl__square"></div>
	</div>
</div>