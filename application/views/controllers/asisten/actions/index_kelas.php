<div class="uk-child-width-1-2@m" uk-grid>
	<div>
		<h1>Kelas Praktikum</h1>
	</div>
	<div>
		<div class="uk-float-right">
			<a href="#modal-add-class" class="uk-button uk-button-primary" uk-toggle>
				<span uk-icon="icon: plus"></span>
				Tambah Kelas
			</a>
		</div>
	</div>
</div>
<div class="uk-child-width-1-2@m" uk-grid>
	<div>
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<div class="uk-grid-small uk-flex-middle" uk-grid>
					<div class="uk-width-auto">
						<img class="uk-border-circle" width="40" height="40" src="images/avatar.jpg">
					</div>
					<div class="uk-width-expand">
						<h3 class="uk-card-title uk-margin-remove-bottom">Title</h3>
						<p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">April 01, 2016</time></p>
					</div>
				</div>
			</div>
			<div class="uk-card-body">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
			</div>
			<div class="uk-card-footer">
				<a href="<?= site_url('plagiat/praktikum') ?>" class="uk-button uk-button-text">Read more</a>
			</div>
		</div>
	</div>
	<div>
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<div class="uk-grid-small uk-flex-middle" uk-grid>
					<div class="uk-width-auto">
						<img class="uk-border-circle" width="40" height="40" src="images/avatar.jpg">
					</div>
					<div class="uk-width-expand">
						<h3 class="uk-card-title uk-margin-remove-bottom">Title</h3>
						<p class="uk-text-meta uk-margin-remove-top"><time datetime="2016-04-01T19:00">April 01, 2016</time></p>
					</div>
				</div>
			</div>
			<div class="uk-card-body">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
			</div>
			<div class="uk-card-footer">
				<a href="<?= site_url('plagiat/praktikum') ?>" class="uk-button uk-button-text">Read more</a>
			</div>
		</div>
	</div>
</div>

<!-- This is the modal -->
<div id="modal-add-class" uk-modal>
	
	<div class="uk-modal-dialog uk-modal-body">
		<form action="" method="post">
			<h2 class="uk-modal-title">Tambah Kelas</h2>
			<fieldset class="uk-fieldset">
				<legend class="uk-legend">Nama Kelas</legend>
				<div class="uk-margin">
					<input class="uk-input" type="text" placeholder="Nama Kelas" name="class_name">
				</div>
			</fieldset>
			<p class="uk-text-right">
				<button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
				<button class="uk-button uk-button-primary" type="button">Save</button>
			</p>
		</form>
	</div>
	
</div>

