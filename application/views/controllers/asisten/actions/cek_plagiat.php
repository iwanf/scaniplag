<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<?php if (isset($un_checked) && $un_checked) : ?>

				<h1 class="uk-text-center">Cek Plagiat</h1>
				<p>Daftar pengecekan plagiat tugas tersubmit </p>
				<table id="table-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Sources</th>
							<th>Bahasa</th>
							<th>Kemiripan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$number =1;
						foreach ($un_checked as $key => $value) { ?>
							<?= form_open('asisten/submit_cek_plagiat/'.$value->id,array('class'=>'form-cek-plagiat','id'=>'form-'.$value->id)) ?>
							<tr>
								<td><?= $number ?></td>
								<td>
									<?= anchor('asisten/details_submit/'.$value->id,$value->sources,array('target'=>'_blank')) ?>
									<input type="hidden" name="sources" value="<?= $value->sources ?>">
								</td>
								<td>
									<?php
									if (isset($languages[$value->language])) {
										echo $languages[$value->language];
									}
									?>
									<input type="hidden" name="language" value="<?= $value->language ?>">	
								</td>
								<td>
									<select name="mml" class="uk-input" data-id="<?= $value->id ?>">
										<option value="0">Pilih</option>
										<?php
										for ($i=1; $i <=12 ; $i++) { ?>
											<option value="<?= $i ?>"><?= $i ?></option>
											<?php 
										}
										?>
									</select>
								</td>
								<td>
									<button class="uk-button uk-button-small uk-button-primary" type="submit" id="submit-<?= $value->id ?>" disabled="true">Submit</button>
								</td>
							</tr>
							<?= form_close() ?>
							<?php	
							$number++;

						}
						?>
					</tbody>
				</table>

				<?php else: ?>
					<div class="uk-text-center" style="padding: 30px;">
						<span class="uk-margin-small-right" data-uk-icon="icon: check; ratio: 6"></span>						
						<h2>
							Semua sudah di Cek
						</h2>
						<p>Untuk melihat hasilnya, tekan tombol dibawah ini</p>
						<?= anchor('asisten/plagiat','Lihat Plagiat',array('class'=>'uk-button uk-button-primary uk-width-1-2'))  ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div id="modal-loading" uk-modal>
		<div class="dl">
			<div class="dl__container">
				<div class="dl__corner--top"></div>
				<div class="dl__corner--bottom"></div>
			</div>
			<div class="dl__square"></div>
		</div>
	</div>