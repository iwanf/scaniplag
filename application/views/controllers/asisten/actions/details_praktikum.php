<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<!-- panel -->
	<div class="uk-width-2-3@l uk-width-1-2@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h3 class="uk-margin-remove-bottom">
							<strong><?= (isset($course['course_name']))? $course['course_name'] : '' ?></strong>							
						</h3>
					</div>

				</div>
			</div>
			<div class="uk-card-body">
				
				<ul class="uk-breadcrumb">
					<li><?= anchor('dashboard/index','Beranda') ?></li>
					<li><?= anchor('asisten/praktikum','Praktikum') ?></li>
					<li class="uk-disabled"><a>
						<?= (isset($course['course_name']))? $course['course_name'] : '' ?>
					</a></li>
				</ul>

				<div class="uk-child-width-1-2@m" uk-grid>
					<div>
						<h4>Modul Praktikum</h4>
					</div>
					<div>

					</div>
				</div>
				<hr/>
				<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul</th>
							<th>Materi</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- /panel -->

	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h4 class="uk-margin-remove-bottom">Details Praktikum</h4>
					</div>
					
				</div>
			</div>
			<div class="uk-card-body">
				<ul class="uk-list">
					<li>
						<strong>Dosen Pengampu</strong><br/>
						<?php
						$dosen = '';
						if (isset($course['lecturer_id']) && !is_null($course['lecturer_id'])) {
							$dosen .=get_info_user($course['lecturer_id'],'first_name').'&nbsp;';
							$dosen .=get_info_user($course['lecturer_id'],'last_name');
						}
						echo ucwords($dosen);
						?>
					</li>
				</ul>
				<strong>Asisten Praktikum</strong>
				<ul class="uk-list uk-list-divider">
					<?php 
					if (isset($asisten)) {
						foreach ($asisten as $key => $value) { ?>
							<li>
								<?php
								
								if (!is_null($value->user_id)) {
									$asisten_praktikum =get_info_user($value->user_id,'first_name').' ';
									$asisten_praktikum .=get_info_user($value->user_id,'last_name').' ';
									echo ucwords($asisten_praktikum);
								}
								

								?>

							</li>
							<?php	
						}
					}
					?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php
if (isset($modules) && $modules) {
	foreach ($modules as $key => $value) { ?>
		<div id="modal-ringkasan-<?= $value->id ?>" uk-modal>
			<div class="uk-modal-dialog uk-modal-body">
				<h2 class="uk-modal-title">Ringkasan Materi </h2>
				<h4><?= $value->title ?></h4>
				<p><?= $value->summary ?></p>
				<div class="uk-text-right">
				
				<button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
			</div>
			</div>

		</div>
	<?php	}
}
?>
