<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<!-- panel -->
	<div class="uk-width-2-3@l uk-width-1-2@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h3 class="uk-margin-remove-bottom">
							<strong><?= (isset($course['course_name']))? $course['course_name'] : '' ?></strong>							
						</h3>
					</div>

				</div>
			</div>
			<div class="uk-card-body">
				
				<ul class="uk-breadcrumb">
					<li><?= anchor('dashboard/index','Beranda') ?></li>
					<li><?= anchor('asisten/plagiat','Plagiat') ?></li>
					<li><?= anchor('asisten/details_submit/'.$plagiat->id,$plagiat->sources) ?></li>
					<li class="uk-disabled"><a>Details Rekaman</a></li>
				</ul>

				<div class="uk-child-width-1-2@m" uk-grid>
					<div>
						<h4>Rekaman Plagiat</h4>
					</div>
					<div>

					</div>
				</div>
				<hr/>
				<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Master</th>
							<th>Pembanding</th>
							<th>Score</th>
							<th>Waktu</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- /panel -->

	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h4 class="uk-margin-remove-bottom">Details Submit</h4>
					</div>
					
				</div>
			</div>
			<div class="uk-card-body">

				<ul class="uk-list uk-list-divider">
					<li>
						<strong>Praktikum</strong><br/>
						<?= (isset($plagiat->course_name))? $plagiat->course_name : '' ?>
					</li>
					<li>
						<strong>Modul</strong><br/>
						<?= (isset($plagiat->title))? $plagiat->title : '' ?>
					</li>
					<li>
						<strong>Bahasa</strong><br/>
						<?= (isset($plagiat->language))? $plagiat->language : '' ?>
					</li>
					<li>
						<strong>Kemiripan</strong><br/>
						<?= (isset($plagiat->mml))? $plagiat->mml : '' ?>
					</li>
					<li>
						<strong>Sources</strong><br/>
						<?= (isset($plagiat->sources))? anchor(base_url('results/'.$plagiat->sources),$plagiat->sources,array('target'=>'_blank')) : '' ?>
					</li>
				</ul>
			</div>
		</div>
	</div>


</div>

<div id="modal-ringkasan" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<p id="txt-ringkasan"></p>
	</div>
</div>