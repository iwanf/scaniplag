<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<!-- panel -->
	<div class="uk-width-2-3@l uk-width-1-2@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h3 class="uk-margin-remove-bottom">
							<strong>
								<?= (isset($module->title))? $module->title : '' ?>
							</strong>							
						</h3>
					</div>
					<div class="uk-width-expand uk-text-right">
						<?php if(isset($module->id)): ?>
							<a href="<?= site_url('admin/edit_modul/'.$module->id) ?>" class="uk-icon-link" data-uk-icon="icon: pencil" title="edit module">
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="uk-card-body">
				<ul class="uk-breadcrumb">
					<li><?= anchor('dashboard/index','Beranda') ?></li>
					<li><?= anchor('admin/praktikum','Praktikum') ?></li>
					<?php
					if (isset($course->course_name) && isset($course->id)) { ?>
						<li><?= anchor('admin/details_praktikum/'.$course->id,ucwords($course->course_name)) ?></li>
					<?php } ?>
					<li class="uk-disabled"><a><?= (isset($module->title))? $module->title : '' ?></a> </li>
				</ul>
				<div class="uk-child-width-1-2@m" uk-grid>
					<div>
						<h4>Daftar Tugas</h4>
					</div>
					<div>
						
					</div>
				</div>
				<hr/>
			</div>
		</div>
	</div>

	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h4 class="uk-margin-remove-bottom">Details Modul</h4>
					</div>
					
				</div>
			</div>
			<div class="uk-card-body">
				<ul class="uk-list uk-list-divider">
					<li>
						<strong>Ringkasan</strong>
						<p><?= (isset($module->summary))? $module->summary : '' ?></p>
					</li>
					<li>
						<strong>Materi</strong>
						<?php if (isset($module->attachment)) { ?>
							<a href="<?= base_url('modules/'.$module->attachment) ?>" target="_blank">
								<?= $module->attachment ?>
							</a>
						<?php } ?>
					</li>
				</ul>
			</div>
		</div>
	</div>

</div>