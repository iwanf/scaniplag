
<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-2 " data-uk-grid >
	<div>
		<h3>Semua Pengguna</h3>
	</div>
	<div uk-margin>
		<?= anchor('admin/export_user','Export',array('class'=>'uk-button uk-button-default')) ?>
	</div>
	
</div>

<div class="uk-card uk-card-default uk-card-small" uk-margin>
	<div class="uk-card-body">
		<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Username</th>
					<th>Email</th>
					<th>Level</th>
					<th>Aksi</th>
				</tr>
			</thead>

		</table>
	</div>
</div>