<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li><?= anchor('admin/praktikum','Praktikum') ?></li>
				<?php
				if (isset($course->course_name) && isset($course->id)) { ?>
					<li><?= anchor('admin/details_praktikum/'.$course->id,ucwords($course->course_name)) ?></li>
				<?php } ?>
				
				<?php  if(isset($module->title)){ ?>
					<li><?= anchor('admin/details_modul/'.$module->id,ucwords($module->title)) ?></li>
				<?php } ?>
				<li class="uk-disabled"><a>Edit Modul</a> </li>
			</ul>
			<h1 class="uk-text-center">Form Edit Modul</h1>
			<?php
			if (isset($module->id)) {
				echo form_open_multipart('admin/submit_edit_modul/'.$module->id);
			}
			?>
			
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Judul</strong>
					<code>*Wajib</code>
					<input type="text" name="title" class="uk-input" required="true" value="<?= (isset($module->title))? $module->title : '' ?>" />
				</label>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Ringkasan</strong>
					<textarea name="summary" class="uk-input"><?= (isset($module->summary))? $module->summary : '' ?></textarea>
				</label>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Materi </strong>

					<code>*.pdf</code>
					<input type="file" name="attachment" accept=".pdf" class="uk-input"/>
					<?php
					if (isset($module->attachment)) { ?>
						<a href="<?= base_url('modules/'.$module->attachment) ?>" target="_blank">Materi Sebelumnya</a>
					<?php	} ?>
				</label>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
				</div>
				<div>
					<div class="uk-text-right">
						<button class="uk-button uk-button-primary" type="submit">Simpan</button>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>