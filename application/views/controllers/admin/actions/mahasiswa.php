<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<div class="uk-width-2-3@l uk-width-1-2@xl">
	</div>
	<!-- panel -->
	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<p uk-margin class="uk-align-right">
			<button class="uk-button uk-button-primary uk-margin-small-right" type="button" uk-toggle="target: #modal-add-class">Tambah Kelas</button>
		</p>
	</div>
	<!-- /panel -->
	<?php
	if (isset($classes)) {
		foreach ($classes as $key => $value) { ?>
			<!-- panel -->
			<div class="uk-width-1-2@s uk-width-1-2@l uk-width-1-4@xl">
				<div class="uk-card uk-card-default uk-card-small">
					<div class="uk-card-header">
						<div class="uk-grid uk-grid-small">
							<div class="uk-width-auto">
								<?php
									$amount = 0;
									if (!is_null($value->amount_students) && $value->amount_students > 0) {
										$amount = $value->amount_students;
									}
								?>

								<?= $amount ?> Mahasiswa</div>
							<div class="uk-width-expand uk-text-right">
								<a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: pencil"></a>
								<?= anchor('admin/daftar_mahasiswa/'.$value->id,'Lihat',array('class'=>'uk-icon-link','data-uk-icon'=>'icon: chevron-right')) ?>
							</div>
						</div>
					</div>
					<div class="uk-card-body">
						<h4 class="uk-margin-remove-bottom">
							<?= $value->class_name ?>
							<br/><small><strong>Dibuat pada</strong>&nbsp;<?= show_date_human_format($value->created_at,true) ?></small>
						</h4>
					</div>
				</div>
			</div>
			<!-- /panel -->
		<?php	}
	}
	?>
	
</div>

<!-- This is the modal -->
<div id="modal-add-class" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<?= form_open('admin/submit_tambah_kelas') ?>
		<h2 class="uk-modal-title">Tambah Kelas</h2>
		<div class="uk-margin">
			<label class="uk-form-label" >Nama Kelas</label>
			<div class="uk-form-controls">
				<input class="uk-input" type="text" name="class_name" placeholder="Tulis Nama kelas">
			</div>
		</div>
		
		<p class="uk-text-right">
			<button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
			<button class="uk-button uk-button-primary" type="submit">Submit</button>
		</p>
		<?= form_close() ?>
	</div>
</div>