<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li class="uk-disabled"><a>Praktikum</a> </li>
			</ul>
			<h1 class="uk-text-center">Daftar Praktikum</h1>
			<div class="uk-text-right">
				<?= anchor('admin/tambah_praktikum','Tambah Praktikum',array('class'=>'uk-button uk-button-secondary')) ?>
			</div>
			<hr/>
			<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Praktikum</th>
						<th>Dosen Pengampu</th>
						<th>Jml. Modul</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
