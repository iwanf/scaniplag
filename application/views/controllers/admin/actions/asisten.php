<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li class="uk-disabled"><a>Asisten</a> </li>
			</ul>
			<h1 class="uk-text-center">Daftar Asisten</h1>
			<div class="uk-text-right">
				<?= anchor('admin/tambah_asisten','Tambah Asisten',array('class'=>'uk-button uk-button-secondary')) ?>
			</div>
			<hr/>
			<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Email</th>
						<th>No. HP</th>
						<th>Nama Lengkap</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
