<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
  <!-- panel -->
  <div class="uk-width-2-3@l uk-width-1-2@xl">
    <div class="uk-card uk-card-default uk-card-small">
      <div class="uk-card-header">
        <div class="uk-grid uk-grid-small">
          <div class="uk-width-auto"><h4 class="uk-margin-remove-bottom">Form Pemindai Plagiat</h4></div>
          <div class="uk-width-expand uk-text-right">
            <a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: move"></a>
            <a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: cog"></a>
            <a href="#" class="uk-icon-link" data-uk-icon="icon: close"></a>
          </div>
        </div>
      </div>
      <div class="uk-card-body">
        <?= form_open_multipart('admin/upload_zip_files') ?>
        <div class="uk-margin" uk-margin>
          <div uk-form-custom="target: true">
            <input type="file" name="files">
            <input class="uk-input uk-form-width-medium" placeholder="Telusuri Zip source code" disabled>
          </div>
          <button class="uk-button uk-button-default">Submit</button>
        </div>
        <?= form_close() ?>
        <table class="uk-table">
          <caption>Source Files</caption>
          <thead>
            <tr>
              <th>Directory</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($sources_dir as $key => $value) : ?>
              <tr>
                <td><?= $value ?></td>
                <td><?= anchor('admin/run_jplag/'.$value,'Cek') ?></td>
              </tr>
            <?php endforeach; ?>
            
          </tbody>
        </table>

      </table>
    </div>
  </div>
</div>
<!-- /panel -->
<!-- panel -->
<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
  <div class="uk-card uk-card-default uk-card-small uk-card-hover">
    <div class="uk-card-header">
      <div class="uk-grid uk-grid-small">
        <div class="uk-width-auto"><h4 class="uk-margin-remove-bottom">Hasil Pemindai</h4></div>
        <div class="uk-width-expand uk-text-right">
          <a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: move"></a>
          <a href="#" class="uk-icon-link uk-margin-small-right" data-uk-icon="icon: cog"></a>
          <a href="#" class="uk-icon-link" data-uk-icon="icon: close"></a>
        </div>
      </div>
    </div>
    <div class="uk-card-body">
      <ul class="uk-list uk-list-divider">
        <li>List item 1</li>
        <li>List item 2</li>
        <li>List item 3</li>
      </ul>
    </div>
  </div>
</div>
<!-- /panel -->
</div>