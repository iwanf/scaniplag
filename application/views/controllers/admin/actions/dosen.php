<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-2 " data-uk-grid >
				<div>
					<h3>Daftar Dosen</h3>
				</div>
				<div>
					<?= anchor('admin/export_dosen','Export',array('class'=>'uk-button uk-button-primary')) ?>
					<button class="uk-button uk-button-default uk-margin-small-right" type="button" uk-toggle="target: #modal-import">Import</button>
				</div>
			</div>

			<div class="uk-card uk-card-default uk-card-small" uk-margin>
				<div class="uk-card-body">
					<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Email</th>
								<th>No. Handphone</th>
								<th>Nama Dosen</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- This is the modal -->
<div id="modal-import" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<?= form_open_multipart('admin/import_add_dosen') ?>
		<h2 class="uk-modal-title">Import Excel</h2>
		<div class="uk-margin">
			<label class="uk-form-label" for="form-stacked-text">Lampiran Excel</label>
			<div class="uk-form-controls">
				<input class="uk-input" id="form-stacked-text" type="file" name="filexlsx" accept=".xlsx" placeholder="Telusuri berkas excel data dosen">
			</div>
		</div>
		
		<p class="uk-text-right">
			<button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
			<button class="uk-button uk-button-primary" type="submit">Submit</button>
		</p>
		<?= form_close() ?>
	</div>
</div>