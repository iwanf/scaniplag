<div class="uk-grid uk-grid-medium uk-grid-match" data-uk-grid>
	<!-- panel -->
	<div class="uk-width-2-3@l uk-width-1-2@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto">
						<h3 class="uk-margin-remove-bottom">
							<strong>
								<?= (isset($asisten['first_name']))? ucfirst($asisten['first_name']) : '' ?>&nbsp;
								<?= (isset($asisten['last_name']))? ucfirst($asisten['last_name']) : '' ?>
							</strong>
						</h3>
					</div>
					
				</div>
			</div>
			<div class="uk-card-body">
				<ul class="uk-breadcrumb">
					<li><?= anchor('dashboard/index','Beranda') ?></li>
					<li><?= anchor('admin/asisten','Asisten') ?></li>
					<li class="uk-disabled"><a>
						<?= (isset($asisten['first_name']))? ucfirst($asisten['first_name']) : '' ?>&nbsp;
						<?= (isset($asisten['last_name']))? ucfirst($asisten['last_name']) : '' ?>
					</a></li>
				</ul>
				<div class="uk-child-width-1-2@m" uk-grid>
					<div>
						<h4>Daftar Praktikum</h4>
					</div>
					<div>
						<div class="uk-text-right">
							<?php if(isset($available) && $available): ?>
								<a href="#modal-add-praktikum" class="uk-button uk-button-secondary"uk-toggle>
									Tambah Praktikum
								</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<hr/>
				<table id="scaniplag-datatables" class="uk-table uk-table-hover uk-table-striped" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Praktikum</th>
							<th>Dosen Pengampu</th>
							<th>Jml. Modul</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- /panel -->
	<!-- panel -->
	<div class="uk-width-1-2@s uk-width-1-3@l uk-width-1-4@xl">
		<div class="uk-card uk-card-default uk-card-small">
			<div class="uk-card-header">
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-auto"><h4 class="uk-margin-remove-bottom">Profile Asisten</h4></div>
        <!--<div class="uk-width-expand uk-text-right">
          <a href="#" class="uk-icon-link" data-uk-icon="icon: pencil" title="edit profile"></a>
      </div>-->
  </div>
</div>
<div class="uk-card-body">
	<ul class="uk-list uk-list-divider">
		<li>
			<strong>Nama Lengkap</strong><br/>
			<?= (isset($asisten['first_name']))? ucfirst($asisten['first_name']) : '' ?>&nbsp;
			<?= (isset($asisten['last_name']))? ucfirst($asisten['last_name']) : '' ?>
		</li>
		<li>
			<strong>Email </strong><br/>
			<?= (isset($asisten['email']))? $asisten['email'] : '' ?>
		</li>
		<li>
			<strong>No. Handphone </strong><br/>
			<?= (isset($asisten['phone']))? $asisten['phone'] : '' ?>
		</li>
	</ul>
</div>
</div>
</div>
<!-- /panel -->
</div>

<div id="modal-add-praktikum" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<?php 
		if (isset($asisten['id'])) {
			echo form_open('admin/input_praktikum/'.$asisten['id']);
		}
		?>
		<h2 class="uk-modal-title">Tambah Praktikum</h2>
		<?php if(isset($available) && $available): ?>
			<?php foreach ($available as $key => $value) { ?>
				
				<div class="uk-margin">
					<input type="checkbox" name="course_id[]" value="<?= $value->id ?>" />
					<label><?= $value->course_name ?></label>
				</div>

			<?php } ?>
			
		<?php endif; ?>
		<p class="uk-text-right">
			<button class="uk-button uk-button-default uk-modal-close" type="button">Batal</button>
			<button class="uk-button uk-button-primary" type="submit">Simpan</button>
		</p>
		<?= form_close() ?>
	</div>
</div>