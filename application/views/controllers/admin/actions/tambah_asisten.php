<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li><?= anchor('admin/asisten','Asisten') ?></li>
				<li class="uk-disabled"><a>Tambah Asisten</a> </li>
			</ul>
			<h1 class="uk-text-center">Form Tambah Asisten</h1>
			<?= form_open('admin/submit_tambah_asisten') ?>
			
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Nama Depan</strong>
							<input type="text" name="first_name" class="uk-input" />
						</label>
					</div>
				</div>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Nama Belakang</strong>
							<input type="text" name="last_name" class="uk-input" />
						</label>
					</div>
				</div>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Email</strong>
					<code>*Wajib</code>
					<input type="email" name="email" class="uk-input" required="true" />
				</label>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>No. Handphone</strong>
					<input type="text" name="phone" class="uk-input" />
				</label>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Password</strong>
							<code>*Wajib</code>
							<input type="password" name="password" class="uk-input" required="true" />
						</label>
					</div>
				</div>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Konfirmasi Password</strong>
							<code>*Wajib</code>
							<input type="password" name="password_confirm" class="uk-input" required="true" />
						</label>
					</div>
				</div>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
				</div>
				<div>
					<div class="uk-text-right">
						<button class="uk-button uk-button-primary" type="submit">Simpan</button>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>