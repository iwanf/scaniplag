<div class="uk-section">
	<div class="uk-container uk-container-small">
		<div class="uk-card uk-card-default uk-card-body">
			<ul class="uk-breadcrumb">
				<li><?= anchor('dashboard/index','Beranda') ?></li>
				<li><?= anchor('admin/praktikum','Praktikum') ?></li>
				<li class="uk-disabled"><a>Edit Praktikum</a> </li>
			</ul>
			<h1 class="uk-text-center">Form Edit Praktikum</h1>
			<?php
				if (isset($course->id)) {
					echo form_open('admin/submit_edit_praktikum/'.$course->id);
				}
			?>
			
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Praktikum</strong>
					<code>*Wajib</code>
					<input type="text" name="course_name" class="uk-input" required="true" value="<?= (isset($course->course_name))? $course->course_name : '' ?>" />
				</label>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">
					<strong>Dosen Pengampu</strong>
					<code>*Wajib</code>
					<select name="lecturer_id" class="uk-input">
						<option value="0">Pilih</option>
						<?php
							if (isset($dosen)) {
								foreach ($dosen as $key => $value) { ?>
								<?php
									if (isset($course->lecturer_id) && $course->lecturer_id == $value->id) { ?>
										<option value="<?= $value->id ?>" selected>
								<?php	}else{ ?>
									<option value="<?= $value->id ?>" >
								<?php	} ?>
								
									<?= $value->first_name ?>
									<?= $value->last_name ?>
								</option>
						<?php	}
							}
						?>
					</select>
				</label>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<strong>Jumlah Modul</strong>
							<code>*Wajib</code>
						</label>
					</div>
				</div>
				<div>
					<div class="uk-margin">
						<label class="uk-form-label">
							<input type="number" name="amount_modules" class="uk-input" value="<?= (isset($course->amount_modules))? $course->amount_modules : '' ?>"/>
						</label>
					</div>
				</div>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>
				<div>
				</div>
				<div>
					<div class="uk-text-right">
						<button class="uk-button uk-button-primary" type="submit">Simpan</button>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>