<?php

/**
* 
*/
require_once APPPATH.'third_party/PHPExcel.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Excel 
{	
	private $ci;
	protected $book = array();	

	function __construct()
	{
		$this->ci =& get_instance();		
	}

	public function read($file)
	{
		$finfo 	= new finfo(FILEINFO_MIME_TYPE);

		if (in_array($finfo->file($file), array(
			'application/octet-stream',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		))) {
			
			$reader 		= new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			
			$worksheetData = $reader->listWorksheetInfo($file);
			foreach ($worksheetData as $worksheet) {
				$reader->setReadDataOnly(true);
				$reader->setLoadSheetsOnly($worksheet['worksheetName']);
				$spreadsheet 	= $reader->load($file);

				
				$catch 		= array();
				$heading 	= array();
				$rows 		= array();
				for ($n=1; $n <= $worksheet['totalRows']; $n++) {
					for($alfa = 'A';$alfa <= $worksheet['lastColumnLetter'] ; $alfa++){
						$key_cell 	= $alfa.$n;
						$cellValue 	= $spreadsheet->getActiveSheet()->getCell($key_cell)->getValue();
						if ($n != 1) {
							$heading = $spreadsheet->getActiveSheet()->getCell($alfa.'1')->getValue();
							$cellValue = trim($cellValue);
							if (!empty($cellValue) || $cellValue == '0') {
								$catch[$heading] = $cellValue;
							}
						}
					}
					if ($catch) {
						$rows[] = $catch;
					}
				}
				if ($rows) {
					$tables 	= $this->get_tables();
					$table_name = trim($worksheet['worksheetName']);
					$table_name = strtolower($table_name);

					if (in_array($table_name, $tables)) {
						$this->book[$table_name] = $rows;
					}else{
						$this->ci->session->set_flashdata('message','sheet '.$table_name.' bukan nama tabel yang sah');	
					}

				}

			}
		}else{
			$this->ci->session->set_flashdata('message','tidak mendukung spreadsheet dengan format '.$finfo->file($file));
		}
		return $this;
	}
	
	public function insert_to_add()
	{
		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;
			$catch = array();
			foreach ($this->book as $key => $value) {
				$catch[] = $value;
			}

			foreach ($this->book as $key => $value) {

				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						if ($this->ci->db->insert($key,$data)) {
							$success++;
						}
					}
					
					if ($all == $success) {
						$match++;
					}else{
						$this->ci->session->set_flashdata('message','berhasil menambahkan '.$success.' data pada tabel '.$key);
					}
				}else{
					$this->ci->session->set_flashdata('message','tidak ada data untuk disisipkan pada tabel '.$key);
				}
			}
			
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import untuk menambahkan data berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import untuk menambahkan data gagal dilakukan :(');
			}
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function insert_to_update()
	{

		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;
			foreach ($this->book as $key => $value) {
				
				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						$exist 			= array();
						$exist_count  	= 0;
						$primary_key  	= $this->get_primary_key($key);

						if(isset($data[$primary_key])){
							$primary_value = $data[$primary_key];	
							$primary_value = trim($primary_value);
							unset($data[$primary_key]);
						}

						if (empty($primary_value) || $primary_value == "") {
							$this->ci->session->set_flashdata('message','primary key tidak ada.');
							$this->ci->go_back();						
						}else{
							
							$this->ci->db->where($primary_key,$primary_value);
							if ($this->ci->db->update($key,$data)) {
								$success++;
							}
						}

					}
					if ($all == $success) {
						$match++;
					}else{
						$this->ci->session->set_flashdata('message','berhasil menambahkan '.$success.' data pada tabel '.$key);
					}
					
				}else{
					$this->ci->session->set_flashdata('message','tidak ada data untuk disisipkan pada tabel '.$key);
				}
			}
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import untuk memperbaharui berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import untuk memperbaharui gagal dilakukan :(');
			}
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function create_format($table_name='',$fields=array())
	{
		$tables     = $this->get_tables();
		$table_name = $this->ci->db->dbprefix.$table_name;
		if (in_array($table_name, $tables)) {
			$heading = $this->ci->db->list_fields($table_name);
			if ($heading) {
				return $this->export_to_xlsx('format_data_'.$table_name,$table_name,$heading);
			}
		}else{
			return false;
		}

	}

	public function create_export_format($table_name='', $fields = array())
	{
		$tables     = $this->get_tables();
		$table_name = $this->ci->db->dbprefix.$table_name;
		if (in_array($table_name, $tables)) {
			if ($fields) {
				$heading = $fields;
			}else{
				$heading = $this->ci->db->list_fields($table_name);
			}
			$data_field = $this->ci->db->get($table_name);
			return $this->export_to_xlsx('export_'.$table_name,$table_name,$heading, $data_field);
		}else{
			return false;
		}
	}

	public function export_to_xlsx($file_name='',$sheet_name='',$data=array(), $data_field = array())
	{
		
		$spreadsheet 	= new Spreadsheet();
		// Create a new worksheet called "My Data"
		$myWorkSheet 	= new \PhpOffice\PhpSpreadsheet\Worksheet($spreadsheet, $sheet_name);

		//delete worksheet
		$sheetIndex = $spreadsheet->getIndex(
			$spreadsheet->getSheetByName('Worksheet')
		);
		$spreadsheet->removeSheetByIndex($sheetIndex);

		//set active sheet
		//$spreadsheet->setActiveSheetIndexByName($sheet_name);

		// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
		$spreadsheet->addSheet($myWorkSheet, 0);
		$sheet 			= $spreadsheet->getSheetByName($sheet_name);//$spreadsheet->getActiveSheet();
		$alfa 			= 'A';
		$no 			= 1;
		foreach ($data as $key => $value) {
			$key_cell 	= $alfa.$no;
			$sheet->setCellValue($key_cell, $value);
			$alfa++;
		}

		// Mengambil Data
		if(isset($data_field) && !empty($data_field)){
			$row = 2;
			foreach($data_field->result() as $field)
			{
				$col = 0;
				foreach ($data as $heading)
				{
					$spreadsheet->setActiveSheetIndexByName($sheet_name)->setCellValueByColumnAndRow($col, $row, $field->$heading);
					$col++;
				}

				$row++;
			}
		}


		$writer = new Xlsx($spreadsheet);
		try {
			if ($writer->save($file_name.'.xlsx')) {
				return true;
			}
			return true;
		}catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
			$this->ci->session->set_flashdata('message','Error loading file: '.$e->getMessage());
			return false;
		}

	}

	public function get_tables()
	{
		$records 		= $this->ci->db->query('SHOW TABLES')->result_array();
		$key_record 	= 'Tables_in_'.$this->ci->db->database;
		$tables 		= array();
		if ($records) {
			foreach ($records as $key => $value) {
				$tables[] 	= $value[$key_record];
			}
		}
		return $tables;
	}

	public function get_primary_key($table_name='')
	{
		$tables = $this->get_tables();
		if (in_array($table_name, $tables)) {
			$query 	= "SHOW KEYS FROM $table_name WHERE Key_name = 'PRIMARY'";
			$result = $this->ci->db->query($query)->row();
			if ($result) {
				return $result->Column_name;
			}
		}
		return false;
	}

	public function insert_to_users()
	{

		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;

			foreach ($this->book as $key => $value) {

				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						$exist_count  	= 0;
						$exist 			= $this->ci->db->get_where($key,array('email'=> $data['email']));
						$exist_count 	= $exist->num_rows();

						if ($exist_count == 0 ) {
							$data['password'] = '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36';
							$data['active'] ='1';
							if ($this->ci->db->insert($key,$data)) {
								$last_id = $this->ci->db->insert_id();
								$success++;
							}
						}else{
							$user = $exist->get()->row();
							$this->ci->db->where('id',$user->id);
							if ($this->ci->db->update($key,$data)) {
								$success++;
							}
						}
					}
					
					if ($all == $success) {
						$match++;
					}
				}else{
					$message ='tidak ada data untuk disisipkan pada tabel '.$key;
					$this->ci->session->set_flashdata('message',$message);
				}
			}
			
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import gagal :(');
			}
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function insert_to_add_lecturers()
	{
		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;
			foreach ($this->book as $key => $value) {
				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						if ($this->ci->db->insert($key,$data)) {
							$last_id = $this->ci->db->insert_id();
							$users['email'] 		= $data['nik'].'@st3telkom.ac.id';
							$users['lecturer_id']	= $last_id;
							$users['username'] 		= $data['nik'];
							$users['password'] 		= '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36';
							$users['company']  		='lecturers';
							$users['active'] 		='1';
							if ($this->ci->db->insert('users',$users)) {
								$user_id = $this->ci->db->insert_id();
								$this->ci->db->insert('users_groups',array(
									'user_id' => $user_id,
									'group_id' => 3
								));
								$success++;
							}
						}
					}
					if ($all == $success) {
						$match++;
					}
				}else{
					$this->ci->session->set_flashdata('message','tidak ada data');
				}
			}
			
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import gagal :(');
			}
			
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function insert_to_add_students($class_id)
	{
		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;
			$success 	= 1;
			foreach ($this->book as $key => $value) {
				if (is_array($value)) {
					$all 		= count($value);
					
					foreach ($value as $index => $data) {
						$data['class_id'] = $class_id;
						if ($this->ci->db->insert($key,$data)) {
							$last_id = $this->ci->db->insert_id();
							$users['email'] 	= $data['nim'].'@st3telkom.ac.id';
							$users['student_id']= $last_id;
							$users['username'] 	= $data['nim'];
							$users['password'] 	= '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36';
							$users['company']  	='students';
							$users['active'] 	='1';
							$this->ci->db->insert('users',$users);
							$success++;
						}
					}
					if ($all == $success) {
						$match++;
					}
				}else{
					$this->ci->session->set_flashdata('message','tidak ada data');
				}
			}
			
			if ($match == $book_amount) {
				$amount_students = $success-1;
				$this->ci->db->where('id',$class_id);
				$this->ci->db->update('students_classes',array('amount_students'=>$amount_students));
				$this->ci->session->set_flashdata('message','import berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import gagal :(');
			}
			
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function insert_to_add_courses()
	{
		$this->ci->load->model('courses');
		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;
			foreach ($this->book as $key => $value) {
				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						if ($this->ci->courses->insert($data)) {
							$success++;
						}
					}
					if ($all == $success) {
						$match++;
					}
				}else{
					$this->ci->session->set_flashdata('message','tidak ada data');
				}
			}
			
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import gagal :(');
			}
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

	public function insert_to_dosen()
	{

		if ($this->book) {
			$book_amount = count($this->book);
			$match 		 = 1;

			foreach ($this->book as $key => $value) {

				if (is_array($value)) {
					$all 		= count($value);
					$success 	= 1;
					foreach ($value as $index => $data) {
						$exist_count  	= 0;
						$exist 			= $this->ci->db->get_where('users',array('email'=> $data['email']));
						$exist_count 	= $exist->num_rows();

						if ($exist_count == 0 ) {
							$data['password'] = '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36';
							$data['active'] ='1';
							if ($this->ci->db->insert('users',$data)) {
								$last_id = $this->ci->db->insert_id();
								$this->ci->db->insert('users_groups',array(
									'user_id' => $last_id,
									'group_id' => 3,
								));
								$success++;
							}
						}else{
							$user = $exist->get()->row();
							$this->ci->db->where('id',$user->id);
							if ($this->ci->db->update('users',$data)) {
								$success++;
							}
						}
					}
					
					if ($all == $success) {
						$match++;
					}
				}else{
					$message ='tidak ada data untuk disisipkan pada tabel '.$key;
					$this->ci->session->set_flashdata('message',$message);
				}
			}
			
			if ($match == $book_amount) {
				$this->ci->session->set_flashdata('message','import berhasil :D');
			}else{
				$this->ci->session->set_flashdata('message','import gagal :(');
			}
		}else{
			$this->ci->session->set_flashdata('message','spreadsheet tidak mendukung :(');
		}
	}

}



