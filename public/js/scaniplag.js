$(document).ready(function() {
  var api_url = $('p.api_url').text();

  if ($('#scaniplag-datatables').length > 0) {
    //console.log(api_url);
  }

  $('#scaniplag-datatables').DataTable({
    "ajax": {
      "url": api_url,
      "type": "POST"
    }
  });    

  $(document).on('click','.btn-confirm',function(){
    if(confirm('Anda sudah yakin? tindakan ini tidak dapat mengembalikan keadaan sebelumnya')){
      return true;
    }
    return false;
  });
 // pilih matakuliah
 $('select[name="course_id" ]').on('change',function(){
  var id        = $(this).val();
  var ajax_url  = $(this).attr('data-url');
  ajax_url  += '/'+id;

  $('select[name="module_id" ]').html('');

  $.ajax({
    type: "GET",
    url: ajax_url,
    dataType: "json",
    cache : false,
    success: function(data) {
      if (data.length > 0) {
        var option = '';
        $.each(data,function(key,value){
          option +='<option value="'+value.id+'">'+value.title+'</option>';
        });

        $('select[name="module_id"]').html(option);
      }else{
        console.log(data.length);
      }
    }
  });
});

 $('#form-submit-tugas').submit(function(e){
  UIkit.modal('#modal-loading').show();
  return true;
});

 $('select[name="mml"]').on('change',function(){
  var id = $(this).attr('data-id');
  if ($(this).val() > 0) {
    $('#submit-'+id).prop('disabled',false);  
  }else{
    $('#submit-'+id).prop('disabled',true);
  }

}); 

 $('.form-cek-plagiat').each(function(key,obj){

  $(obj).submit(function(e){
    UIkit.modal('#modal-loading').show();
    return true;
  });
});


});